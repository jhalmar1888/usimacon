<?php
use Illuminate\Http\Request;

Route::get('test' , function(){
    $p = App\Models\Persona::find(1);
    $p->registraRequisitos();
});

Route::get('testWord' , function(){
    $pe = App\Models\PersonaEspecialidad::find(1);
    $pe->enviaRequisitosPorCorreo();
    dd("procesado");
    //dd($pe->generaDocumento());
    //dd($pe->imprimeRequisitos()); 
});

Route::get('/', function () {
    if(Auth::check())
        return view('welcome');
    else 
        return redirect(route('login'));
})->name('home');

Route::post('login/tokenLoginVerify', ['uses' => 'Auth\LoginController@tokenLoginVerify', 'as' => 'login.tokenLoginVerify'])->middleware('guest');
Auth::routes();

Route::post('uploadFile', function (Request $request) {
    try {
        if ($request->hasFile('File')) {
            $fileName = md5(uniqid() . \Carbon\Carbon::now()) . '.' . strtolower($request->file('File')->getClientOriginalExtension());
            $path = $request->file('File')->storeAs('documents', $fileName, 'documents');
            $data = array(
                'success' => true,
                'data' => $fileName,
                'msg' => trans('messages.file_uplodaded')
            );
        } else {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => 'Error al guardar archivo.'
            );
        }
    } catch (\Exception $e) {
        $data = array(
            'success' => false,
            'data' => null,
            'msg' => $e->getMessage()
        );
    }
    return response()->json($data);
})->name('utils.uploadFile');

Route::group(['prefix' => 'Persona', 'middleware' => 'auth'], function () {
        Route::get('/view', ['uses' => 'PersonaController@view', 'as' => 'Persona.view']);
        Route::get('/list', ['uses' => 'PersonaController@list', 'as' => 'Persona.list']);
        Route::get('/index', ['uses' => 'PersonaController@index', 'as' => 'Persona.index']);
        Route::get('/show', ['uses' => 'PersonaController@show', 'as' => 'Persona.show']);
        Route::post('/destroy', ['uses' => 'PersonaController@destroy', 'as' => 'Persona.destroy']);
        Route::post('/store', ['uses' => 'PersonaController@store', 'as' => 'Persona.store']);
        Route::get('/profile', ['uses' => 'PersonaController@profile', 'as' => 'Persona.profile']);
        Route::get('/imprimirRegistro', ['uses' => 'PersonaController@imprimirRegistro', 'as' => 'Persona.imprimirRegistro']);
});

Route::group(['prefix' => 'PersonaDocumento', 'middleware' => 'auth'], function () {
        Route::get('/view', ['uses' => 'PersonaDocumentoController@view', 'as' => 'PersonaDocumento.view']);
        Route::get('/list', ['uses' => 'PersonaDocumentoController@list', 'as' => 'PersonaDocumento.list']);
        Route::get('/index', ['uses' => 'PersonaDocumentoController@index', 'as' => 'PersonaDocumento.index']);
        Route::post('/destroy', ['uses' => 'PersonaDocumentoController@destroy', 'as' => 'PersonaDocumento.destroy']);
        Route::post('/store', ['uses' => 'PersonaDocumentoController@store', 'as' => 'PersonaDocumento.store']);
        Route::get('/download', ['uses' => 'PersonaDocumentoController@download', 'as' => 'PersonaDocumento.download']);
});

Route::group(['prefix' => 'PersonaRelacion', 'middleware' => 'auth'], function () {
        Route::get('/view', ['uses' => 'PersonaRelacionController@view', 'as' => 'PersonaRelacion.view']);
        Route::get('/list', ['uses' => 'PersonaRelacionController@list', 'as' => 'PersonaRelacion.list']);
        Route::get('/index', ['uses' => 'PersonaRelacionController@index', 'as' => 'PersonaRelacion.index']);
        Route::post('/destroy', ['uses' => 'PersonaRelacionController@destroy', 'as' => 'PersonaRelacion.destroy']);
        Route::post('/store', ['uses' => 'PersonaRelacionController@store', 'as' => 'PersonaRelacion.store']);
        Route::get('/show', ['uses' => 'PersonaRelacionController@show', 'as' => 'PersonaRelacion.show']);
});

Route::group(['prefix' => 'PersonaCargo', 'middleware' => 'auth'], function () {
    Route::get('/view', ['uses' => 'PersonaCargoController@view', 'as' => 'PersonaCargo.view']);
    Route::get('/list', ['uses' => 'PersonaCargoController@list', 'as' => 'PersonaCargo.list']);
    Route::get('/index', ['uses' => 'PersonaCargoController@index', 'as' => 'PersonaCargo.index']);
    Route::post('/destroy', ['uses' => 'PersonaCargoController@destroy', 'as' => 'PersonaCargo.destroy']);
    Route::post('/store', ['uses' => 'PersonaCargoController@store', 'as' => 'PersonaCargo.store']);
    Route::get('/show', ['uses' => 'PersonaCargoController@show', 'as' => 'PersonaCargo.show']);
});

Route::group(['prefix' => 'PersonaSector', 'middleware' => 'auth'], function () {
    Route::get('/view', ['uses' => 'PersonaSectorController@view', 'as' => 'PersonaSector.view']);
    Route::get('/list', ['uses' => 'PersonaSectorController@list', 'as' => 'PersonaSector.list']);
    Route::get('/index', ['uses' => 'PersonaSectorController@index', 'as' => 'PersonaSector.index']);
    Route::post('/destroy', ['uses' => 'PersonaSectorController@destroy', 'as' => 'PersonaSector.destroy']);
    Route::post('/store', ['uses' => 'PersonaSectorController@store', 'as' => 'PersonaSector.store']);
    Route::get('/show', ['uses' => 'PersonaSectorController@show', 'as' => 'PersonaSector.show']);
});


/*============================= ROL ***********************************************************************************/
Route::group(['prefix' => 'Rol', 'middleware' => 'auth'], function () {
        Route::get('/view',        ['uses' => 'RolController@view', 'as'     => 'Rol.view']);
        Route::get('/list',       ['uses' => 'RolController@list', 'as'     => 'Rol.list']);
        Route::get('/index',       ['uses' => 'RolController@index', 'as'    => 'Rol.index']);
        Route::post('/destroy',    ['uses' => 'RolController@destroy', 'as'  => 'Rol.destroy']);
        Route::post('/store',      ['uses' => 'RolController@store', 'as'    => 'Rol.store']);
        Route::get('/show',       ['uses' => 'RolController@show', 'as'     => 'Rol.show']);
});   

/*============================= Incripción ***********************************************************************************/
Route::group(['prefix' => 'Inscripcion'], function () {
    Route::post('/store', ['uses' => 'InscripcionController@store', 'as' => 'Inscripcion.store']);
    Route::get('/verify/{tokenVerificacion}', ['uses' => 'InscripcionController@verify', 'as' => 'Inscripcion.verify']);
});     

  // Route::group(['prefix' => 'Inscripcion'], function () {
    Route::get('pre-inscripcion', ['uses' => 'InscripcionController@view', 'as' => 'Inscripcion.view']);
    Route::post('pre-inscripcion', ['uses' => 'InscripcionController@preinscripcion', 'as' => 'Preinscripcion.store']);
    Route::get('registro', function() {
        return view('modules.Inscripcion.index');
    });
// });
/*============================= CARGO ***********************************************************************************/
Route::group(['prefix' => 'Cargo', 'middleware' => 'auth'], function () {
        Route::get('/view',        ['uses' => 'CargoController@view', 'as'     => 'Cargo.view']);
        Route::get('/list',        ['uses' => 'CargoController@list', 'as'     => 'Cargo.list']);
        Route::get('/index',       ['uses' => 'CargoController@index', 'as'    => 'Cargo.index']);
        Route::post('/destroy',    ['uses' => 'CargoController@destroy', 'as'  => 'Cargo.destroy']);
        Route::post('/store',      ['uses' => 'CargoController@store', 'as'    => 'Cargo.store']);
        Route::post('/show',        ['uses' => 'CargoController@show', 'as'     => 'Cargo.show']);
});  

/*============================= Tipo Relacion ***********************************************************************************/
Route::group(['prefix' => 'TipoRelacion', 'middleware' => 'auth'], function () {
        Route::get('view',        ['uses' => 'TipoRelacionController@view', 'as'     => 'TipoRelacion.view']);
        Route::get('list',        ['uses' => 'TipoRelacionController@list', 'as'     => 'TipoRelacion.list']);
        Route::get('index',       ['uses' => 'TipoRelacionController@index', 'as'    => 'TipoRelacion.index']);
        Route::post('destroy',    ['uses' => 'TipoRelacionController@destroy', 'as'  => 'TipoRelacion.destroy']);
        Route::post('store',      ['uses' => 'TipoRelacionController@store', 'as'    => 'TipoRelacion.store']);
        Route::post('show',        ['uses' => 'TipoRelacionController@show', 'as'     => 'TipoRelacion.show']);
});  

/*============================= Sector ***********************************************************************************/
Route::group(['prefix' => 'Sector', 'middleware' => 'auth'], function () {
        Route::get('view',        ['uses' => 'SectorController@view', 'as'     => 'Sector.view']);
        Route::get('list',        ['uses' => 'SectorController@list', 'as'     => 'Sector.list']);
        Route::get('index',       ['uses' => 'SectorController@index', 'as'    => 'Sector.index']);
        Route::post('destroy',    ['uses' => 'SectorController@destroy', 'as'  => 'Sector.destroy']);
        Route::post('store',      ['uses' => 'SectorController@store', 'as'    => 'Sector.store']);
        Route::post('show',        ['uses' => 'SectorController@show', 'as'     => 'Sector.show']);
        Route::get('imprimirSector', ['uses' => 'SectorController@imprimirSector', 'as' => 'Sector.imprimirSector']);

});  

/*============================= Tipo Vehiculo ***********************************************************************************/
Route::group(['prefix' => 'TipoVehiculo', 'middleware' => 'auth'], function () {
        Route::get('/view',        ['uses' => 'TipoVehiculoController@view', 'as'     => 'TipoVehiculo.view']);
        Route::get('/list',        ['uses' => 'TipoVehiculoController@list', 'as'     => 'TipoVehiculo.list']);
        Route::get('/index',       ['uses' => 'TipoVehiculoController@index', 'as'    => 'TipoVehiculo.index']);
        Route::post('/destroy',    ['uses' => 'TipoVehiculoController@destroy', 'as'  => 'TipoVehiculo.destroy']);
        Route::post('/store',      ['uses' => 'TipoVehiculoController@store', 'as'    => 'TipoVehiculo.store']);
        Route::post('/show',        ['uses' => 'TipoVehiculoController@show', 'as'     => 'TipoVehiculo.show']);
});  

/*============================= Gestion ***********************************************************************************/
Route::group(['prefix' => 'Gestion', 'middleware' => 'auth'], function () {
        Route::get('/view',        ['uses' => 'GestionController@view', 'as'     => 'Gestion.view']);
        Route::get('/list',        ['uses' => 'GestionController@list', 'as'     => 'Gestion.list']);
        Route::get('/index',       ['uses' => 'GestionController@index', 'as'    => 'Gestion.index']);
        Route::post('/destroy',    ['uses' => 'GestionController@destroy', 'as'  => 'Gestion.destroy']);
        Route::post('/store',      ['uses' => 'GestionController@store', 'as'    => 'Gestion.store']);
        Route::post('/show',        ['uses' => 'GestionController@show', 'as'     => 'Gestion.show']);
        Route::post('/generatePagos',        ['uses' => 'GestionController@generatePagos', 'as'     => 'Gestion.generatePagos']);
});  


/*============================= Persona  Movilidad ***********************************************************************************/
Route::group(['prefix' => 'PersonaMovilidad', 'middleware' => 'auth'], function () {
    Route::get('/view',        ['uses' => 'PersonaMovilidadController@view', 'as'     => 'PersonaMovilidad.view']);
    Route::get('/list',        ['uses' => 'PersonaMovilidadController@list', 'as'     => 'PersonaMovilidad.list']);
    Route::get('/index',       ['uses' => 'PersonaMovilidadController@index', 'as'    => 'PersonaMovilidad.index']);
    Route::post('/destroy',    ['uses' => 'PersonaMovilidadController@destroy', 'as'  => 'PersonaMovilidad.destroy']);
    Route::post('/store',      ['uses' => 'PersonaMovilidadController@store', 'as'    => 'PersonaMovilidad.store']);
    Route::post('/show',        ['uses' => 'PersonaMovilidadController@show', 'as'     => 'PersonaMovilidad.show']);
    Route::post('/activar',        ['uses' => 'PersonaMovilidadController@activar', 'as'     => 'PersonaMovilidad.activar']);

});  

/*============================= Persona  Movilidad Pago ***********************************************************************************/
Route::group(['prefix' => 'PersonaPago', 'middleware' => 'auth'], function () {
    Route::get('/view',        ['uses' => 'PersonaPagoController@view', 'as'     => 'PersonaPago.view']);
    Route::get('/list',        ['uses' => 'PersonaPagoController@list', 'as'     => 'PersonaPago.list']);
    Route::get('/index',       ['uses' => 'PersonaPagoController@index', 'as'    => 'PersonaPago.index']);
    Route::post('/destroy',    ['uses' => 'PersonaPagoController@destroy', 'as'  => 'PersonaPago.destroy']);
    Route::post('/store',      ['uses' => 'PersonaPagoController@store', 'as'    => 'PersonaPago.store']);
    Route::post('/show',        ['uses' => 'PersonaPagoController@show', 'as'     => 'PersonaPago.show']);
    Route::post('/pagar',        ['uses' => 'PersonaPagoController@pagar', 'as'     => 'PersonaPago.pagar']);
    Route::post('/condonar',        ['uses' => 'PersonaPagoController@condonar', 'as'     => 'PersonaPago.condonar']);
    Route::get('/imprimir',        ['uses' => 'PersonaPagoController@imprimir', 'as'     => 'PersonaPago.imprimir']);
    Route::get('/gestionPagos',        ['uses' => 'PersonaPagoController@gestionPagos', 'as'     => 'PesonaPago.gestionPago']);

});  

/*============================= Reporte ***********************************************************************************/
Route::group(['prefix' => 'Reporte', 'middleware' => 'auth'], function () {
    Route::get('/view',        ['uses' => 'ReporteController@view', 'as'     => 'Reporte.view']);
    Route::post('/generate',        ['uses' => 'ReporteController@generate', 'as'     => 'Reporte.generate']);
});  

