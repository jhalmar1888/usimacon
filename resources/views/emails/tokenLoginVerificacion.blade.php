<div class="email-container">
    <div class="email-title">
        {{ config('app.name') }}
    </div>
    <div class="email-body">
        <p>
            <i>
                <strong>Hola {{ $user->Usuario}}, </strong>
            </i>
        </p>
        <p>Se ha generado una clave para inicio de sesión:</p>
        <h1><strong>{{$tokenLogin}}</strong></h1>
    </div>
</div>