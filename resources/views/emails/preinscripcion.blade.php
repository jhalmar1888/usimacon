<div class="email-container">
    <div class="email-title">
        {{ config('app.name') }}
    </div>
    <div class="email-body">
        <p>
            <i>
                <strong>Hola {{ $persona->Persona}}, </strong>
            </i>
        </p>
        <p>Tu solicitud de preinscripcion para la especialidad {{ $especialidad->Especialidad }} ha sido satisfactoria</p>
        <p>Adjunto a este correo encontraras los formularios y requisitos necesarios</p>
    </div>
</div>