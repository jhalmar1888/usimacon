<html lang="{{ config('app.locale')}}">
    <head>
        <meta charset="utf-8">
        <title>{{ config('app.name') }}</title>
</head>
<body style="background-color: #011B58 ">

<table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;" align="center">
	<tr>
		<td style="background-color: #ecf0f1" align="center">
			<div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
				<h2 style="color: #e67e22; margin: 0 0 7px" align="center">{{ config('app.name') }}</h2>
				<div style="width: 100%;margin:20px 0; display: inline-block;text-align: center">
					<img style="padding: 0; width: 200px; margin: 5px" src="https://www.emi.edu.bo/images/Logos_EMI/Escudo-Bolivia-EMI.jpg">
				</div>
				<div style="width: 100%; text-align: center">
						<div class="wrapper-page" style="width: 80% !important;">
							<div class="card card-pages">
								<div class="card-body">
									<h3 class="text-center m-t-0 m-b-15">
										Hola {{$persona->Nombre}} {{$persona->ApPaterno}} {{$persona->ApMaterno}},
										para confirmar su preinscripción deberá hacer click en el siguiente enlace. <br>
										<a href="{{url('')}}/Inscripcion/verify/{{$tokenVerificacion}}  "> Haga Click Aquí</a>
									</h3>

								</div>

							</div>
						</div>



				</div>
				
			</div>
		</td>
	</tr>
</table>
<!--hasta aquí-->

</body>
</html>
