<div id="sidebar-menu">
    <ul>
        <li class="active">
            <a href="{{ route('home') }}" class="waves-effect {{ Request::is('/') ? 'active' : '' }}"><i class="fas fa-home"></i><span> {{ trans('labels.modules.Inicio') }}</a>        
        </li>
        @if(Auth::user()->Rol==1)
        <li class="active">
            <a href="{{ route('Persona.view') }}" class="waves-effect {{ Request::is('Persona/view*') ? 'active' : '' }}"><i class="fas fa-user-alt"></i><span> {{ trans('labels.modules.Persona') }}</a>        
        </li>
        <li class="active">
            <a href="{{ url('PersonaMovilidad/view') }}" class="waves-effect {{ Request::is('PersonaMovilidad*') ? 'active' : '' }}"><i class="fa fa-car"></i><span>Búsqueda por Placa </a>
        </li>
        @endif
            <li class="active">
                <a href="{{ url('Gestion/view') }}" class="waves-effect {{ Request::is('Gestion*') ? 'active' : '' }}"><i class="fa fa-calendar"></i><span>{{ trans_choice('labels.modules.Gestion', 1)}} </a>
            </li>
            <li class="active">
                <a href="{{ url('Cargo/view') }}" class="waves-effect {{ Request::is('Cargo*') ? 'active' : '' }}"><i class="fa fa-cubes"></i><span>Cargos</a>
            </li>
            <li class="active">
                <a href="{{ url('Sector/view') }}" class="waves-effect {{ Request::is('Sector*') ? 'active' : '' }}"><i class="fa fa-sitemap"></i><span>Sectores </a>
            </li>
            <li class="active">
                <a href="{{ url('TipoRelacion/view') }}" class="waves-effect {{ Request::is('TipoRelacion*') ? 'active' : '' }}"><i class="fa fa-check"></i><span>Tipo Relacion Familia </a>
            </li>
            <li class="active">
                <a href="{{ url('TipoVehiculo/view') }}" class="waves-effect {{ Request::is('TipoVehiculo*') ? 'active' : '' }}"><i class="fa fa-car"></i><span>Tipos de Vehiculos </a>
            </li>
            <li class="active">
                <a href="{{ url('Reporte/view') }}" class="waves-effect {{ Request::is('Reporte*') ? 'active' : '' }}"><i class="far fa-file-pdf"></i><span>Reportes </a>
            </li>
    </ul>
</div>