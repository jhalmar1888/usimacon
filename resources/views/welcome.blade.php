@extends('layouts.app')
@section('content')

<div id="dashboard-app">

<div class="content">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-4">
            <img src="{{url('')}}/images/emi_logo.png" alt="Advertisement" border="0" width="50%" >
        </div>
    </div>
    <BR>
    <BR> <BR>
    <BR> <BR>
    </div>
    <div class="page-content-wrapper ">
        <div class="container-fluid">
            <BR> <BR>
            <div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <div class="card text-center">
                                <div class="card-heading">
                                    <h4 class="card-title text-muted font-weight-light mb-0">Total de Afiliados</h4>
                                </div>
                                <div class="card-body p-t-10">
                                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi mdi-account m-r-10"></i><b>@{{ afiliados }}</b></h2>
                                    <!--<p class="text-muted m-b-0 m-t-20"><b>48%</b> From Last 24 Hours</p>-->
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-4">
                            <div class="card text-center">
                                <div class="card-heading">
                                    <h4 class="card-title text-muted font-weight-light mb-0">Total de Movilidades</h4>
                                </div>
                                <div class="card-body p-t-10">
                                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi mdi-bus-school m-r-10"></i><b>@{{ movilidades }}</b></h2>
                                    <!--<p class="text-muted m-b-0 m-t-20"><b>42%</b> Orders in Last 10 months</p>-->
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-4">
                            <div class="card text-center">
                                <div class="card-heading">
                                    <h4 class="card-title text-muted font-weight-light mb-0">Tipos de Vehiculos</h4>
                                </div>
                                <div class="card-body p-t-10">
                                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi mdi-bus m-r-10"></i><b>@{{ tipoVehiculo }} </b></h2>
                                    <!--<p class="text-muted m-b-0 m-t-20"><b>22%</b> From Last 24 Hours</p>-->
                                </div>
                            </div>
                        </div>

                        <!--<div class="col-sm-6 col-lg-3">
                            <div class="card text-center">
                                <div class="card-heading">
                                    <h4 class="card-title text-muted font-weight-light mb-0">Monthly Earnings</h4>
                                </div>
                                <div class="card-body p-t-10">
                                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi-arrow-down-bold-circle-outline text-danger m-r-10"></i><b>5621</b></h2>
                                    <p class="text-muted m-b-0 m-t-20"><b>35%</b> From Last 1 Month</p>
                                </div>
                            </div>
                        </div>-->
                    </div>
            </div>
               
        </div>
    </div>
</div>
</div>
<!--
y completar el dashboard con cuantos afiliados - 
                          cuantas movilidades  -
                          cuantos tipos de movilidades  

                          grafico por sector - grafico por gestion cobro - 
                      -->
<script>
    var auth = {!! Auth::user() !!};
    var urlListAfiliado             = '{!! route('Persona.list')!!}';
    var urlListPersonaMovilidad     = '{!! route('PersonaMovilidad.list')!!}';
    var urlListTipoVehiculo         = '{!! route('TipoVehiculo.list')!!}';
    var urlListPersonaPagoGestion        = '{!! route('PesonaPago.gestionPago')!!}';

</script>

{!! Html::script('/js/Dashboard/Dashboard.js') !!}

@endsection
 