@extends('layouts.app')
@section('content')
<div id="sector-app">
    <div class="content">
        <div class="">
            <div class="page-header-title">
                <h4 class="page-title">{{ trans('labels.modules.Sector') }}</h4>
            </div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header miClase">
                                <h3 class="card-title text-dark m-0">Registros</h3>
                                <div class="btn-group float-right">
                                    <a href="#" @click.prevent="newSector" class="btn btn-success waves-effect waves-light m-l-10"><i class="fa fa-plus"></i> {{ trans('labels.actions.new')}}</a>
                                </div>
                            </div>
                            <div class="card-body miClase">
                                <table id="sector-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de sector-->
    <div class="modal fade" id="frm-sector" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>{{ trans('labels.modules.Sector') }}</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="Num">Número</label>
                            <input type="text" class="form-control" name="Num" v-model="sector.Num">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Num"><li class="parsley-required">@{{ errorBag.Num }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Sector">Sector</label>
                            <input type="text" class="form-control" name="Sector" v-model="sector.Sector">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sector"><li class="parsley-required">@{{ errorBag.Sector }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="ZonaSector">Zona Sector</label>
                            <input type="text" class="form-control" name="ZonaSector" v-model="sector.ZonaSector">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sector"><li class="parsley-required">@{{ errorBag.ZonaSector }}</li></ul>
                        </div>
 
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="saveSector()" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>

     <!-- vista de sector-->
    <div class="modal fade" id="view-sector" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title m-0" id="custom-width-modalLabel">{{ trans('labels.modules.Sector') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>Número</h4>
                    <p class="text-muted">@{{ sector.Num }}</p>
                    <h4>Sector</h4>
                    <p class="text-muted">@{{ sector.Sector }}</p>
                    <h4>Zona Sector</h4>
                    <p class="text-muted">@{{ sector.ZonaSector }}</p>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="imprimirSector" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i> {{ trans('labels.actions.sect') }}</a>
                    <a href="#" @click.prevent="editSector"   class="btn btn-warning"><i class="fa fa-edit"></i> {{ trans('labels.actions.edit') }}</a>
                    <a href="#" @click.prevent="deleteSector" class="btn btn-danger"><i class="fa fa-trash"></i> {{ trans('labels.actions.destroy') }}</a>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script>
    var auth = {!! Auth::user() !!};
    var urlIndexSector   = '{!! route('Sector.index')!!}';
    var urlShowSector    = '{!! route('Sector.show')!!}';
    var urlSaveSector    = '{!! route('Sector.store')!!}';
    var urlDestroySector = '{!! route('Sector.destroy')!!}';
    var urlImprimirSector = '{!! route('Sector.imprimirSector')!!}';
</script>
{!! Html::script('/js/Sector/Sector.js') !!}
@endsection
