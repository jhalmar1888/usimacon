@extends('layouts.app')
@section('content') 



<div id="persona-app">
    <loading v-if="isLoading"></loading>
    <div class="content">
        <div class="">
            <div class="page-header-title">
                <h4 class="page-title">{{ trans('labels.modules.Persona') }}</h4>
            </div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-dark m-0">Detalle de Registros</h3>
                                <div class="btn-group float-right">
                                    <a href="#" @click.prevent="newPersona" class="btn btn-success waves-effect waves-light m-l-10"><i class="fa fa-plus"></i> {{ trans('labels.actions.new')}}</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="persona-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de persona-->
    <div class="modal fade animated zoomIn" id="frm-persona" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 10px 20px 0 20px !important;">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>{{ trans('labels.modules.Persona') }}</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="Nombre">Nombre(s)</label>
                                    <input type="text" class="form-control" name="Nombre" v-model="persona.Nombre">                                                   
                                    <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Nombre"><li class="parsley-required">@{{ errorBag.Nombre }}</li></ul>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label for="ApPaterno"> Apellido Paterno</label>
                                <input type="text" class="form-control" name="ApPaterno" v-model="persona.ApPaterno">
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.ApPaterno"><li class="parsley-required">@{{ errorBag.ApPaterno }}</li></ul>
                            </div>
                            <div class="col-lg-3">
                                <label for="ApMaterno"> Apellido Materno</label>
                                <input type="text" class="form-control" name="ApMaterno" v-model="persona.ApMaterno">
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.ApMaterno"><li class="parsley-required">@{{ errorBag.ApMaterno }}</li></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="ApMaterno">Carnet de Identidad</label>
                                    <input type="text" class="form-control" name="CI" v-model="persona.CI">
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.CI"><li class="parsley-required">@{{ errorBag.ApMaterno }}</li></ul>
                                </div>
                                <div class="col-lg-2">
                                    <label for="Expedido"> Expedido en:</label>
                                    <select type="text" class="form-control" name="Rol" v-model="persona.Expedido">
                                        <option :value="e.id" v-for="e in expedidos">@{{ e.Expedido }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Expedido"><li class="parsley-required">@{{ errorBag.Expedido }}</li></ul>
                                </div>
                                <div class="col-lg-3">
                                    <label for="FechaNacimiento"> Fecha de Nacimiento </label>
                                    <div class="input-group">
                                        <input type="text" autocomplete="off" class="form-control datepicker" name="FechaNacimiento" id="FechaNacimiento" v-model="persona.FechaNacimiento">
                                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar"></i></span>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.FechaNacimiento"><li class="parsley-required">@{{ errorBag.FechaNacimiento }}</li></ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                        <label for="Sexo"> Sexo</label> <br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="Sexo" id="SexoM" value="M" checked="" v-model="persona.Sexo">
                                            <label class="form-check-label" for="defaultInlineRadio1"><i class="fa fa-male"></i> Masculino</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="Sexo" id="SexoF" value="F" checked="" v-model="persona.Sexo">
                                            <label class="form-check-label" for="defaultInlineRadio1"><i class="fa fa-female"></i>  Femenino</label>
                                        </div>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sexo"><li class="parsley-required">@{{ errorBag.Sexo }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="Expedido"> Grupo Sanguineo:</label>
                                    <select type="text" class="form-control" name="GrupoSanguineo" v-model="persona.GrupoSanguineo">
                                        <option :value="gs.id" v-for="gs in grupoSanguineos">@{{ gs.GrupoSanguineo }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.GrupoSanguineo"><li class="parsley-required">@{{ errorBag.GrupoSanguineo }}</li></ul>
                                </div>
                                <div class="col-md-3">
                                    <label for="EstadoCivil"> Estado Civil:</label>
                                    <select type="text" class="form-control" name="EstadoCivil" v-model="persona.EstadoCivil">
                                        <option :value="ec.id" v-for="ec in estadoCivils">@{{ ec.EstadoCivil }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.EstadoCivil"><li class="parsley-required">@{{ errorBag.EstadoCivil }}</li></ul>
                                </div>
                                <div class="col-md-3">
                                    <label for="Telefono">Teléfono</label>
                                    <input type="text" class="form-control" name="Telefono" v-model="persona.Telefono">
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Telefono"><li class="parsley-required">@{{ errorBag.Telefono }}</li></ul>
                                </div>
                                <div class="col-md-3">
                                    <label for="Telefono">Celular</label>
                                    <input type="text" class="form-control" name="Celular" v-model="persona.Celular">
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Celular"><li class="parsley-required">@{{ errorBag.Celular }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for=""> Categoría Licencia(s): </label><br>
                                    <div class="checkbox checkbox-primary form-check-inline" v-for="cl in categoriaLicencias">
                                        <input type="checkbox" :id="cl.id" :name="cl.id" v-model="persona.CategoriaLicencia" :value="cl.id"> 
                                        <label :for="cl.id"> @{{ cl.CategoriaLicencia }} </label> 
                                    </div>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.CategoriaLicencia"><li class="parsley-required">@{{ errorBag.CategoriaLicencia }}</li></ul>
                                </div>
                                <div class="col-lg-4">
                                    <label for="SectorActual"> Sector Actual:</label>
                                    <select type="text" class="form-control" name="SectorActual" v-model="persona.SectorActual">
                                        <option :value="s.id" v-for="s in sectors">@{{ s.Sector }}- @{{ s.ZonaSector }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sector"><li class="parsley-required">@{{ errorBag.SectorActual }}</li></ul>
                                </div>
                                  <div class="col-lg-4">
                                    <label for="FechaIngreso"> Fecha de Ingreso al Sindicato </label>
                                    <div class="input-group">
                                        <input type="text" autocomplete="off" class="form-control datepicker" name="FechaIngreso" id="FechaIngreso" v-model="persona.FechaIngreso">
                                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar"></i></span>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.FechaIngreso"><li class="parsley-required">@{{ errorBag.FechaIngreso }}</li></ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkbox checkbox-primary">
                                        <input id="checkbox2" type="checkbox" v-model="persona.Activo">
                                        <label for="checkbox2">
                                            Activo
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="TipoSocio"> Tipo de Socio:</label>
                                    <select type="text" class="form-control" name="TipoSocio" v-model="persona.TipoSocio">
                                        <option :value="ts.id" v-for="ts in tipoSocios">@{{ ts.TipoSocio }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.TipoSocio"><li class="parsley-required">@{{ errorBag.TipoSocio }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="Direccion">Dirección</label>
                                    <textarea class="form-control" name="Direccion" v-model="persona.Direccion"></textarea>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Direccion"><li class="parsley-required">@{{ errorBag.Direccion }}</li></ul>
                                </div>
                                <div class="col-md-6">
                                    <label for="Observaciones">Observaciones</label>
                                    <textarea class="form-control" name="Observaciones" v-model="persona.Observaciones"></textarea>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <div class="row">

                            </div>
                        </div>-->
                        <hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="Rol">Rol</label>
                                    <select type="text" class="form-control" name="Rol" v-model="persona.Rol">
                                        <option :value="r.id" v-for="r in roles">@{{ r.Rol }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Rol"><li class="parsley-required">@{{ errorBag.Rol }}</li></ul>
                                </div>
                                <div class="col-lg-4">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" v-model="persona.email">
                                    <ul class="parsley-errors-list filled" id="parsley-|id-19" v-if="errorBag.email"><li class="parsley-required">@{{ errorBag.email }}</li></ul> 
                                </div>
                                <div class="col-lg-4">
                                    <label for="password">Contraseña</label>
                                    <input type="password" class="form-control" name="password" v-model="persona.password">
                                    <ul class="parsley-errors-list filled" id="parsley-|id-19" v-if="errorBag.password"><li class="parsley-required">@{{ errorBag.password }}</li></ul> 
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="Antecedente">Antecedentes Sindicales</label>
                                    <textarea class="form-control" name="Antecedente" v-model="persona.Antecedente"></textarea>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Antecedente"><li class="parsley-required">@{{ errorBag.Antecedente }}</li></ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="padding-top: 2px !important">
                    <a href="#" @click.prevent="savePersona()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>
      <!-- vista de Persona-->
    <div class="modal fade" id="view-persona" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title m-0" id="custom-width-modalLabel">{{ trans('labels.modules.Persona') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div><b>Nombre Completo: </b>@{{ persona.Persona }}</div>
                            <div><b>Carnet de Identidad: </b>@{{ persona.CI }} @{{ persona.Expedido }}</div>
                            <div><b>Fecha de Nacimiento: </b>@{{ moment(persona.FechaNacimiento).format('DD-MM-Y') }}</div>
                            <div><b>Sexo: </b>@{{ persona.Sexo == 'M' ? 'Masculino' : 'Femenino' }}</div>
                            <div><b>Estado Civil: </b>@{{ persona.EstadoCivil }}</div>
                            <div><b>Teléfono: </b>@{{ persona.Telefono }}</div>
                            <div><b>Celular: </b>@{{ persona.Celular }}</div>
                            <div><b>Grupo Sanguineo: </b>@{{ persona.GrupoSanguineo }}</div>
                            <div><b>Categoría Licencia: </b>@{{ persona.CategoriaLicencia }}</div>
                            <div><b>Dirección: </b>@{{ persona.Direccion }}</div>
                            <div><b>Sector Actual: </b>@{{ persona.sector_actual ? persona.sector_actual.Sector : '' }}</div>
                            <div><b>Tipo Socio: </b>@{{ persona.TipoSocio }}</div>
                            <div><b>Fecha Ingreso al Sindicato: </b>@{{ moment(persona.FechaIngreso).format('DD-MM-Y') }}</div>
                            <div><b>Observaciones: </b>@{{ persona.Observaciones }}</div>
                            <div><b>Activo: </b>@{{ persona.Activo ? 'Sí' : 'No' }}</div>
                            <div><b>Antecedentes sindicales: </b><textarea rows="1" cols="50" disabled>@{{ persona.Antecedente }}</textarea></div>
                        </div>
                        <div class="col-md-4">
                            <img :src="persona.FotografiaUrl" alt="" style="width:180px;">
                            <div class="form-group m-b-0">
                                <p>Fotografía:</p>
                                <input type="file" class="filestyle" id="Archivo" data-buttonname="btn-primary" data-buttontext="Seleccionar..." @change="uploadImage">
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Archivo"><li class="parsley-required">@{{ errorBag.Archivo }}</li></ul>
                            </div>
                            <span v-show="isLoadingFile" class="text-success"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Cargando Archivo<span class="sr-only">Cargando...</span></span>
                            <span v-show="!isLoadingFile && persona.Fotografia" class="text-info"><i class="fa fa-thumbs-o-up"></i> Archivo Cargado!</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="imprimirRegistro" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i> {{ trans('labels.actions.print') }}</a>
                    <a href="#" @click.prevent="goProfile" class="btn btn-info"><i class="fa fa-user"></i> {{ trans('labels.actions.profile') }}</a>
                    <a href="#" @click.prevent="editPersona" class="btn btn-warning"><i class="fa fa-edit"></i> {{ trans('labels.actions.edit') }}</a>
                    <a href="#" @click.prevent="deletePersona" class="btn btn-danger"><i class="fa fa-trash"></i> {{ trans('labels.actions.destroy') }}</a>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>

<script>
    var auth = {!! Auth::user() !!};
    var urlIndexPersona     = '{!! route('Persona.index') !!}';
    var urlShowPersona      = '{!! route('Persona.show') !!}';
    var urlSavePersona      = '{!! route('Persona.store') !!}';
    var urlDestroyPersona   = '{!! route('Persona.destroy') !!}';
    var urlImprimirRegistro = '{!! route('Persona.imprimirRegistro')!!}';
    var urlListRol = '{!! route('Rol.list')!!}';
    var urlListSector  = '{!! route('Sector.list') !!}';
    
    var urlUploadFile = '{!! route('utils.uploadFile') !!}';

    $(document).ready(function() {
        $('#select_multiple').select2();
    });
</script>



{!! Html::script('/js/Persona/Persona.js') !!}
@endsection
