@extends('layouts.app')
@section('content') 
<div id="persona-app">
    <loading v-if="isLoading"></loading>
    <div class="content">
        <div class="">
            <div class="page-header-title">
                <h4 class="page-title">{{ trans('labels.modules.Persona') }}</h4>
            </div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-dark m-0">Perfil de @{{persona.Persona }}</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div><b>Nombre Completo: </b>@{{ persona.Persona }}</div>
                                        <div><b>Carnet de Identidad: </b>@{{ persona.CI }} @{{ persona.Expedido }}</div>
                                        <div><b>Fecha de Nacimiento: </b>@{{ moment(persona.FechaNacimiento).format('DD-MM-Y') }}</div>
                                        <div><b>Sexo: </b>@{{ persona.Sexo == 'M' ? 'Masculino' : 'Femenino' }}</div>
                                        <div><b>Estado Civil: </b>@{{ persona.EstadoCivil }}</div>
                                        <div><b>Teléfono: </b>@{{ persona.Telefono }}</div>
                                        <div><b>Celular: </b>@{{ persona.Celular }}</div>
                                        <div><b>Antecedentes sindicales: </b><textarea rows="1" cols="50" disabled>@{{ persona.Antecedente }}</textarea></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div><b>Grupo Sanguineo: </b>@{{ persona.GrupoSanguineo }}</div>
                                        <div><b>Categoría Licencia: </b>@{{ persona.CategoriaLicencia }}</div>
                                        <div><b>Dirección: </b>@{{ persona.Direccion }}</div>
                                        <div><b>Sector Actual: </b>@{{ persona.sector_actual ? persona.sector_actual.Sector : '' }}</div>
                                        <div><b>Tipo Socio: </b>@{{ persona.TipoSocio }}</div>
                                        <div><b>Fecha Ingreso al Sindicato: </b>@{{ moment(persona.FechaIngreso).format('DD-MM-Y') }}</div>
                                        <div><b>Observaciones: </b>@{{ persona.Observaciones }}</div>
                                        <div><b>Activo: </b>@{{ persona.Activo ? 'Sí' : 'No' }}</div>
                                    </div>
                                    <div class="col-md-4">
                                        <!-- <img src="/images/default_image_profile.png" alt="" style="width:180px;"> -->
                                        <img :src="persona.FotografiaUrl" alt="" style="width:180px;">
                                    </div>
                                    <a href="#" @click.prevent="imprimirRegistro" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i> {{ trans('labels.actions.print') }}</a>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="tabs-vertical-env">
                                        <ul class="nav nav-tabs flex-column nav tabs-vertical" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="v-familiares-tab" data-toggle="tab" href="#v-familiares" role="tab" aria-controls="v-familiares" aria-selected="true" @click="getPersonaRelacion"><i class="fa fa-users"></i> Familiares</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="v-documentos-tab" data-toggle="tab" href="#v-documentos" role="tab" aria-controls="v-documentos" aria-selected="false" @click="getPersonaDocumento"><i class="fa fa-suitcase"></i> Documentos</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="v-cargos-tab" data-toggle="tab" href="#v-cargos" role="tab" aria-controls="v-cargos" aria-selected="false" @click="getPersonaCargo"><i class="fa fa-address-card"></i> Cargos</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="v-sector-tab" data-toggle="tab" href="#v-sector" role="tab" aria-controls="v-sector" aria-selected="false" @click="getPersonaSector"><i class="fa fa-sitemap"></i> Sectores</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="v-movilidades-tab" data-toggle="tab" href="#v-movilidades" role="tab" aria-controls="v-movilidades" aria-selected="false" @click="getPersonaMovilidad"><i class="fa fa-car"></i> Movilidades</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="v-pagos-tab" data-toggle="tab" href="#v-pagos" role="tab" aria-controls="v-pagos" aria-selected="false" @click="getPersonaPago"><i class="fas fa-money-bill-wave"></i> Pagos</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-light">
                                            <div class="tab-pane fade show active" id="v-familiares" role="tabpanel" aria-labelledby="v-familiares-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" @click.prevent="newPersonaRelacion" class="btn btn-success btn-xs float-right"><i class="fa fa-plus"></i> {{ trans('labels.actions.new') }}</a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nombre Completo</th>
                                                                    <th>Tipo de Relación</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="personaRelacions.length > 0" v-for="pr in personaRelacions">
                                                                    <td>@{{ pr.PersonaRelacion }}</td>
                                                                    <td>@{{ pr.tipo_relacion ? pr.tipo_relacion.TipoRelacion : '' }}</td>
                                                                    <td>
                                                                        <a href="#" @click.prevent="editPersonaRelacion(pr)" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                                                        <a href="#" @click.prevent="deletePersonaRelacion(pr)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>                                                    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="tab-pane fade" id="v-documentos" role="tabpanel" aria-labelledby="v-documentos-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" @click.prevent="newPersonaDocumento" class="btn btn-success btn-xs float-right"><i class="fa fa-plus"></i> {{ trans('labels.actions.new') }}</a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Documento</th>
                                                                    <th>Presentado</th>
                                                                    <th>Observaciones</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="personaDocumentos.length > 0" v-for="pd in personaDocumentos">
                                                                    <td>@{{ pd.PersonaDocumento }}</td>
                                                                    <td>
                                                                        <span v-if="pd.Presentado"><i class="fa fa-check text-success"></i></span>
                                                                        <span v-else><i class="fa fa-ban text-danger"></i></span>
                                                                    </td>
                                                                    <td>@{{ pd.Observaciones }}</td>
                                                                    <td>
                                                                        <a href="#" v-if="pd.Archivo" @click.prevent="downloadPersonaDocumento(pd)" class="btn btn-xs btn-info"><i class="fa fa-download"></i></a>
                                                                        <a href="#" @click.prevent="editPersonaDocumento(pd)" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                                                        <a href="#" @click.prevent="deletePersonaDocumento(pd)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>                                                    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="tab-pane fade" id="v-cargos" role="tabpanel" aria-labelledby="v-cargos-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" @click.prevent="newPersonaCargo" class="btn btn-success btn-xs float-right"><i class="fa fa-plus"></i> {{ trans('labels.actions.new') }}</a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Cargo</th>
                                                                    <th>Tipo de Cargo</th>
                                                                    <th>Fecha Inicio</th>
                                                                    <th>Fecha Fin</th>
                                                                    <th>Observaciones</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="personaCargos.length > 0" v-for="pc in personaCargos">
                                                                    <td>@{{ pc.cargo ? pc.cargo.Cargo : '' }}</td>
                                                                    <td>@{{ pc.cargo ? pc.cargo.TipoCargo : '' }}</td>
                                                                    <td>@{{ pc.FechaInicio }}</td>
                                                                    <td>@{{ pc.FechaFin }}</td>
                                                                    <td>@{{ pc.Observaciones }}</td>
                                                                    <td>
                                                                        <a href="#" @click.prevent="editPersonaCargo(pc)" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                                                        <a href="#" @click.prevent="deletePersonaCargo(pc)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>                                                    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-sector" role="tabpanel" aria-labelledby="v-sector-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" @click.prevent="newPersonaSector" class="btn btn-success btn-xs float-right"><i class="fa fa-plus"></i> {{ trans('labels.actions.new') }}</a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sector</th>
                                                                    <th>Zona Sector</th>
                                                                    <th>Fecha Inicio</th>
                                                                    <th>Fecha Fin</th>
                                                                    <th>Observaciones</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="personaSectors" v-for="ps in personaSectors">
                                                                    <td>@{{ ps.sector ? ps.sector.Sector : ''  }}</td>
                                                                    <td>@{{ ps.sector ? ps.sector.ZonaSector : ''  }}</td>
                                                                    <td>@{{ ps.FechaInicio }}</td>
                                                                    <td>@{{ ps.FechaFin }}</td>
                                                                    <td>@{{ ps.Observaciones }}</td>
                                                                    <td>
                                                                        <a href="#" @click.prevent="editPersonaSector(ps)" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                                                        <a href="#" @click.prevent="deletePersonaSector(ps)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>                                                    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-movilidades" role="tabpanel" aria-labelledby="v-movilidades-tab">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" @click.prevent="newPersonaMovilidad" class="btn btn-success btn-xs float-right"><i class="fa fa-plus"></i> {{ trans('labels.actions.new') }}</a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Tipo Vehículo </th>
                                                                    <th>Marca</th>
                                                                    <th>Capacidad</th>
                                                                    <th>Modelo</th>
                                                                    <th>Placa</th>
                                                                    <th>Chasis</th>
                                                                    <th>Color</th>
                                                                    <th>Observaciones</th>
                                                                    <th>Radicatoria</th>
                                                                    <th>Activo</th>
                                                                    <th>Estado de Revisión</th>
                                                                    <th>Foto</th>
                                                                    <th>Opciones</th>
                                                                
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="personaMovilidades.length > 0" v-for="pm in personaMovilidades">
                                                                    <td>@{{ pm.tipo_vehiculo ? pm.tipo_vehiculo.TipoVehiculo : '' }}</td>
                                                                    <td>@{{ pm.Marca }}</td>
                                                                    <td>@{{ pm.Capacidad }}</td>
                                                                    <td>@{{ pm.Modelo }}</td>
                                                                    <td>@{{ pm.Placa }}</td>
                                                                    <td>@{{ pm.NumeroChasis }}</td>
                                                                    <td>@{{ pm.Color }}</td>
                                                                    <td>@{{ pm.Observaciones }}</td>
                                                                    <td>@{{ pm.Radicatoria }}</td>
                                                                    <td v-if = "pm.Activo" >
                                                                    <i class="fa fa-check text-success"></i>
                                                                    </td>
                                                                    <td v-else = "false"><i class="fa fa-ban text-danger"></i></td>
                                                                    <td v-if = "pm.Revisado" >
                                                                    <i class="fa fa-check text-success"></i>
                                                                    </td>
                                                                    <td v-else = "false"><i class="fa fa-ban text-danger"></i></td>
                                                                    <td v-if="pm.Fotografiamovi" > 
                                                                    <img :src="pm.FotografiamoviUrl" class="avatar-circle" width='500' heigth='450' alt="profile_picture"> 
                                                                    <td v-else= "null">sin figura</td>
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" @click.prevent="editPersonaMovilidad(pm)" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                                                        <a href="#" @click.prevent="deletePersonaMovilidad(pm)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                                                        <a href="#" @click.prevent="activarPersonaMovilidad(pm)" :class="'btn btn-xs ' + (pm.Revisado ? 'btn-danger' : 'btn-info')"><i :class="'fa ' + (pm.Estado ? 'far fa-eye-slash' : 'fas fa-eye')"></i></a>

                                                                    </td>
                                                                </tr>                                                    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>  
                                            </div>
                                             <div class="tab-pane fade" id="v-pagos" role="tabpanel" aria-labelledby="v-pagos-tab">
                                                <div class="row">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4">
                                                        <form class="form" role="form">
                                                            <div class="form-group">
                                                                <label for="Gestion">Gestión: </label> 
                                                                <select name="Gestion" id="Gestion" class="form-control" v-model="gestion" @change="getPersonaPago">
                                                                    <option :value="g.id" v-for="g in gestions">@{{ g.Gestion }}</option>
                                                                </select>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="#" @click.prevent="newPersonaPago" class="btn btn-success btn-xs float-right"><i class="fa fa-plus"></i> {{trans('labels.actions.new')}}</a>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Gestión</th>
                                                                    <th>Mes</th>
                                                                    <th>Monto</th>
                                                                    <th>Pagado</th>
                                                                    <th>Condonado</th>
                                                                    <th>Recibo</th>
                                                                    <th>Observaciones</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr v-if="personaPagos.length > 0" v-for="pp in personaPagos">
                                                                    <td>@{{ pp.Gestion }}</td>
                                                                    <td>@{{ pp.Mes }}</td>
                                                                    <td>@{{ accounting.formatMoney(pp.Monto, "", 2, ".", ",") }}</td>
                                                                    <td><i v-if="pp.Pagado" class="fa fa-check text-succcess"></i><i v-else class="fa fa-ban text-danger"></i></td>
                                                                    <td><i v-if="pp.Condonado" class="fa fa-check text-succcess"></i><i v-else class="fa fa-ban text-danger"></i></td>
                                                                    <td>@{{ pp.NumeroRecibo }}</td>
                                                                    <td>@{{ pp.Observaciones }}</td>
                                                                    <td>
                                                                        <template v-if="!(pp.Pagado || pp.Condonado)">
                                                                            <a href="#" @click.prevent="pagarPago(pp)" class="btn btn-xs btn-success"><i class="far fa-money-bill-alt"></i> Pagar</a>
                                                                            <a href="#" @click.prevent="condonarPago(pp)" class="btn btn-xs btn-info"><i class="fa fa-star"></i> Condonar</a>
                                                                        </template>
                                                                        <a v-else href="#" @click.prevent="imprimirPago(pp)" class="btn btn-xs btn-warning"><i class="fa fa-print"></i> imprimir</a>
                                                                        <a href="#" @click.prevent="editPersonaPago(pp)" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>
                                                                    </td>
                                                                </tr>                                                    
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de personaRelacion-->
    <div class="modal fade" id="frm-personaRelacion" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Registrar Familiar</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="PersonaRelacion">Nombre(s) y Apellido(s)</label>
                                    <input type="text" class="form-control" name="PersonaRelacion" v-model="personaRelacion.PersonaRelacion">                                                   
                                    <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.PersonaRelacion"><li class="parsley-required">@{{ errorBag.PersonaRelacion }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="TipoRelacion"> Tipo de Relación Familiar:</label>
                                    <select type="text" class="form-control" name="TipoRelacion" v-model="personaRelacion.TipoRelacion">
                                        <option :value="tr.id" v-for="tr in tipoRelacions">@{{ tr.TipoRelacion }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.TipoRelacion"><li class="parsley-required">@{{ errorBag.TipoRelacion }}</li></ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="savePersonaRelacion()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de personaDocumento-->
    <div class="modal fade" id="frm-personaDocumento" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Registrar Documento</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="PersonaDocumento">Documento</label>
                                    <input type="text" class="form-control" name="PersonaDocumento" v-model="personaDocumento.PersonaDocumento">                                                   
                                    <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.PersonaDocumento"><li class="parsley-required">@{{ errorBag.PersonaDocumento }}</li></ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group m-b-0">
                                        <p>Archivo:</p>
                                        <input type="file" class="filestyle" id="Archivo" data-buttonname="btn-primary" data-buttontext="Seleccionar..." @change="uploadFile">
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Archivo"><li class="parsley-required">@{{ errorBag.Archivo }}</li></ul>
                                    </div>
                                    <span v-show="isLoadingFile" class="text-success"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Cargando Archivo<span class="sr-only">Cargando...</span></span>
                                    <span v-show="!isLoadingFile && persona.Fotografia" class="text-info"><i class="fa fa-thumbs-o-up"></i> Archivo Cargado!</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox2" type="checkbox" v-model="personaDocumento.Presentado">
                                        <label for="checkbox2">Presentado</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="Observaciones">Observaciones</label>
                                    <textarea class="form-control" name="Observaciones" v-model="personaDocumento.Observaciones"></textarea>                                              
                                    <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="savePersonaDocumento()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>

     <!-- formulario de personaCargo-->
    <div class="modal fade" id="frm-personaCargo" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Registrar De Cargos</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="Cargo"> Cargo:</label>
                                    <select type="text" class="form-control" name="Cargo" v-model="personaCargo.Cargo">
                                        <option :value="c.id" v-for="c in cargos">@{{ c.Cargo }} - @{{ c.TipoCargo }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Cargo"><li class="parsley-required">@{{ errorBag.Cargo }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="FechaInicio"> Fecha de Inicio </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="FechaInicio" id="FechaInicio" autocomplete="off">
                                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar"></i></span>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.FechaInicio"><li class="parsley-required">@{{ errorBag.FechaInicio }}</li></ul>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="FechaFin"> Fecha Final </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="FechaFin" id="FechaFin" autocomplete="off">
                                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar"></i></span>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.FechaFin"><li class="parsley-required">@{{ errorBag.FechaFin }}</li></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <label for="Observaciones">Observaciones</label>
                                    <textarea class="form-control" name="Observaciones" v-model="personaCargo.Observaciones"></textarea>                                              
                                    <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="savePersonaCargo()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>
     <!-- formulario de personaSector-->
    <div class="modal fade" id="frm-personaSector" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Registrar Sector</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="Sector"> Sector:</label>
                                    <select type="text" class="form-control" name="Sector" v-model="personaSector.Sector">
                                        <option :value="s.id" v-for="s in sectors">@{{ s.Sector }} - @{{ s.ZonaSector }}</option>
                                    </select>
                                    <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sector"><li class="parsley-required">@{{ errorBag.Sector }}</li></ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                           <div class="row">
                                <div class="col-lg-6">
                                    <label for="FechaInicio"> Fecha de Inicio </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="FechaInicio" id="FechaInicioa" autocomplete="off">
                                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar"></i></span>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.FechaInicio"><li class="parsley-required">@{{ errorBag.FechaInicio }}</li></ul>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="FechaFin"> Fecha Final </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="FechaFin" id="FechaFina" autocomplete="off">
                                        <span class="input-group-addon bg-custom b-0"><i class="mdi mdi-calendar"></i></span>
                                        <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.FechaFin"><li class="parsley-required">@{{ errorBag.FechaFin }}</li></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <label for="Observaciones">Observaciones</label>
                                    <textarea class="form-control" name="Observaciones" v-model="personaSector.Observaciones"></textarea>                                              
                                    <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="savePersonaSector()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de personaMovilidad-->
    
    <div class="modal fade animated zoomIn" id="frm-personaMovilidad" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Registrar Movilidad</strong></h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="TipoVehiculo">Tipo  Vehículo</label>
                                <select type="text" class="form-control" name="TipoVehiculo" v-model="personaMovilidad.TipoVehiculo">
                                    <option :value="tv.id" v-for="tv in tipoVehiculos">@{{ tv.TipoVehiculo }}</option>
                                </select>
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.TipoVehiculo"><li class="parsley-required">@{{ errorBag.TipoVehiculo }}</li></ul>
                            </div>
                            <div class="col-md-4">
                                <label for="Marca">Marca</label>
                                <input type = "text" class="form-control" name="Marca" v-model="personaMovilidad.Marca">                   
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Marca }}</li></ul>
                            </div>
                             <div class="col-md-4">
                                <label for="Modelo">Modelo</label>
                                <input type = "number" class="form-control" name="Modelo" v-model="personaMovilidad.Modelo">
                                                        
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Modelo"><li class="parsley-required">@{{ errorBag.Modelo }}</li></ul>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-md-4">
                                <label for="Capacidad">Capacidad</label>
                                <input type ="text" class="form-control" name="Capacidad" v-model="personaMovilidad.Capacidad">
                            </div>
                            <div class="col-md-4">
                                <label for="Color">Color</label>
                                <input type ="text" class="form-control" name="Color" v-model="personaMovilidad.Color">
                            </div>
                            <div class="col-md-6">
                                <label for="NumeroChasis">Número Chasis</label>
                                <input type ="text" class="form-control" name="NumeroChasis" v-model="personaMovilidad.NumeroChasis">
                                                        
                            </div>
                            <div class="col-md-4">
                                <label for="Placa">Placa</label>
                                <input type = "text" class="form-control" name="Placa" v-model="personaMovilidad.Placa">
                                                        
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Placa"><li class="parsley-required">@{{ errorBag.Placa }}</li></ul>
                            </div>
                        
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="checkbox checkbox-success">
                                    <input id="checkbox3" type="checkbox" v-model="personaMovilidad.Activo">
                                    <label for="checkbox3">Activo</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="Radicatoria">Radicatoria</label>
                                <select type="text" class="form-control" name="Radicatoria" v-model="personaMovilidad.Radicatoria">
                                    <option :value="ra.id" v-for="ra in radicatorias">@{{ ra.Radicatoria }}</option>
                                </select>
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Radicatoria"><li class="parsley-required">@{{ errorBag.Radicatoria }}</li></ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="Observaciones">Observaciones</label>
                                <textarea class="form-control" name="Observaciones" v-model="personaMovilidad.Observaciones"></textarea>                                              
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <img :src="personaMovilidad.FotografiamoviUrl" alt="" style="width:330px;">
                            <div class="form-group m-b-0">
                                <p>Fotografía de la Movilidad:</p>
                                <input type="file" class="filestyle" id="Fotografiamovi" data-buttonname="btn-primary" data-buttontext="Seleccionar..." @change="uploadImageMovi">
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Archivo"><li class="parsley-required">@{{ errorBag.Archivo }}</li></ul>
                            </div>
                            <span v-show="isLoadingFile" class="text-success"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Cargando Archivo<span class="sr-only">Cargando...</span></span>
                            <span v-show="!isLoadingFile && personaMovilidad.Fotografiamovi" class="text-info"><i class="fa fa-thumbs-o-up"></i> Archivo Cargado!</span>
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="savePersonaMovilidad()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de personaPago-->
    
    <div class="modal fade animated zoomIn" id="frm-personaPago" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Registrar Pago</strong></h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="TipoVehiculo">Gestión:</label>
                                <select type="text" class="form-control" name="Gestion" v-model="personaPago.Gestion" :disabled="personaPago.id">
                                    <option :value="g.id" v-for="g in gestions">@{{ g.Gestion }}</option>
                                </select>
                                <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Gestion"><li class="parsley-required">@{{ errorBag.Gestion }}</li></ul>
                            </div>
                            <div class="col-md-4">
                                <label for="MarcMesa">Mes:</label>
                                <input type = "text" class="form-control" name="Mes" v-model="personaPago.Mes" :disabled="personaPago.id">                   
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Mes"><li class="parsley-required">@{{ errorBag.Mes }}</li></ul>
                            </div>
                             <div class="col-md-4">
                                <label for="Monto">Monto</label>
                                <input type = "number" class="form-control" name="Monto" v-model="personaPago.Monto" :disabled="personaPago.id">
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Monto"><li class="parsley-required">@{{ errorBag.Monto }}</li></ul>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-md-12">
                                <label for="Observaciones">Observaciones</label>
                                <textarea class="form-control" name="Observaciones" v-model="personaPago.Observaciones"></textarea>                                              
                                <ul class="parsley-errors-list filled"  id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                            </div>
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="savePersonaPago()" class="btn btn-success">{{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" @click.prevent="getPersonaPago" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var auth = {!! Auth::user() !!};
    var persona = {!! $persona !!};
    var urlShowPersona = '{!! route('Persona.show') !!}';
    var urlImprimirRegistro = '{!! route('Persona.imprimirRegistro')!!}';

    var urlListPersonaRelacion = '{!! route('PersonaRelacion.list') !!}';
    var urlSavePersonaRelacion = '{!! route('PersonaRelacion.store') !!}';
    var urlDestroyPersonaRelacion = '{!! route('PersonaRelacion.destroy') !!}';
    var urlListTipoRelacion  = '{!! route('TipoRelacion.list') !!}';
    
    var urlListPersonaDocumento = '{!! route('PersonaDocumento.list') !!}';
    var urlSavePersonaDocumento = '{!! route('PersonaDocumento.store') !!}';
    var urlDestroyPersonaDocumento = '{!! route('PersonaDocumento.destroy') !!}';
    var urlDownloadPersonaDocumento = '{!! route('PersonaDocumento.download') !!}';

    var urlListPersonaCargo = '{!! route('PersonaCargo.list') !!}';
    var urlSavePersonaCargo = '{!! route('PersonaCargo.store') !!}';
    var urlDestroyPersonaCargo = '{!! route('PersonaCargo.destroy') !!}';
    var urlListCargo  = '{!! route('Cargo.list') !!}';

    var urlListPersonaSector = '{!! route('PersonaSector.list') !!}';
    var urlSavePersonaSector = '{!! route('PersonaSector.store') !!}';
    var urlDestroyPersonaSector = '{!! route('PersonaSector.destroy') !!}';
    var urlListSector  = '{!! route('Sector.list') !!}';
    
    var urlListPersonaMovilidad = '{!! route('PersonaMovilidad.list') !!}';
    var urlSavePersonaMovilidad = '{!! route('PersonaMovilidad.store') !!}';
    var urlDestroyPersonaMovilidad = '{!! route('PersonaMovilidad.destroy') !!}';
    var urlActivarPersonaMovilidad = '{!! route('PersonaMovilidad.activar') !!}';   
    var urlListTipoVehiculo = '{!! route('TipoVehiculo.list')!!}';
    
    var urlListPersonaPago = '{!! route('PersonaPago.list') !!}';
    var urlSavePersonaPago = '{!! route('PersonaPago.store') !!}';
    var urlDestroyPersonaPago = '{!! route('PersonaPago.destroy') !!}';
    var urlPagarPersonaPago = '{!! route('PersonaPago.pagar') !!}';
    var urlCondonarPersonaPago = '{!! route('PersonaPago.condonar') !!}';
    var urlImprimirPersonaPago = '{!! route('PersonaPago.imprimir') !!}';
    var urlListGestion = '{!! route('Gestion.list')!!}';

    var urlUploadFile = '{!! route('utils.uploadFile') !!}';




</script>
{!! Html::script('/js/Persona/Profile.js') !!}
@endsection
