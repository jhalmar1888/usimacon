@extends('layouts.app')
@section('content')
<div id="reporte-app">
    <div class="content">
        <div class="">
            <div class="page-header-title">
                <h4 class="page-title">{{ trans('labels.modules.Reporte') }}</h4>
            </div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-dark m-0">Registros</h3>
                            </div>
                            <div class="card-body">
                                <table id="sector-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <tr>
                                        <td>Reporte</td>
                                        <td></td>
                                    </tr>
                                    <tr v-for="reporte in reportes">
                                        <td>@{{reporte.Reporte}}</td>
                                        <td><a href="#" @click="generateReporte(reporte)" class="btn btn-info"><i class="fas fa-cloud-download-alt"></i> {{ trans('labels.actions.download') }}</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var auth = {!! Auth::user() !!};
    var urlGenerateReporte   = '{!! route('Reporte.generate')!!}';
</script>
{!! Html::script('/js/Reporte/Reporte.js') !!}
@endsection
