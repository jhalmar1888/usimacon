@extends('layouts.app')
@section('content')
<div id="gestion-app">
    <div class="content">
        <div class="">
            <div class="page-header-title">
                <h4 class="page-title">{{ trans_choice('labels.modules.Gestion', 1) }}</h4>
            </div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-dark m-0">Registros</h3>
                                <div class="btn-group float-right">
                                    <a href="#" @click.prevent="newGestion" class="btn btn-success waves-effect waves-light m-l-10"><i class="fa fa-plus"></i> {{ trans('labels.actions.new')}}</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="gestion-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de gestion-->
    <div class="modal fade" id="frm-gestion" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>{{ trans_choice('labels.modules.Gestion', 1) }}</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="Gestion">Gestion</label>
                            <input type="number" class="form-control" name="Gestion" v-model="gestion.Gestion">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Gestion"><li class="parsley-required">@{{ errorBag.Gestion }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Monto">Monto</label>
                            <input type="text" class="form-control" name="Monto" v-model="gestion.Monto">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Monto"><li class="parsley-required">@{{ errorBag.Monto }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Observaciones">Observaciones</label>
                            <textarea class="form-control" name="Observaciones" v-model="gestion.Observaciones"></textarea>
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Observaciones"><li class="parsley-required">@{{ errorBag.Observaciones }}</li></ul>
                        </div>
                            <strong><label>ESTOS CAMPOS SON PARA GENERAR NOMBRES EN LAS FIRMAS POR GESTION PARA EL REPORTE DE FILIACION</label></strong>
                        <div class="form-group">
                            <label for="Sgeneral">STRIO. GENERAL</label>
                            <input class="form-control" name="Sgeneral" v-model="gestion.Sgeneral">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sgeneral"><li class="parsley-required">@{{ errorBag.Sgeneral }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Shacienda">STRIO. DE HACIENDA</label>
                            <input class="form-control" name="Shacienda" v-model="gestion.Shacienda">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Shacienda"><li class="parsley-required">@{{ errorBag.Shacienda }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Sconflicto">STRIO. DE CONFLICTOS</label>
                            <input class="form-control" name="Sconflicto" v-model="gestion.Sconflicto">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sconflicto"><li class="parsley-required">@{{ errorBag.Sconflicto }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Sregimen">STRIO. REGIMEN INTERNO</label>
                            <input class="form-control" name="Sregimen" v-model="gestion.Sregimen">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Sregimen"><li class="parsley-required">@{{ errorBag.Sregimen }}</li></ul>
                        </div>
 
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="saveGestion()" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>

     <!-- vista de gestion-->
    <div class="modal fade" id="view-gestion" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title m-0" id="custom-width-modalLabel">{{ trans_choice('labels.modules.Gestion', 1) }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>Gestion</h4>
                    <p class="text-muted">@{{ gestion.Gestion }}</p>
                    <h4>Monto</h4>
                    <p class="text-muted">@{{ gestion.Monto }}</p>
                    <h4>Observaciones</h4>
                    <p class="text-muted">@{{ gestion.Observaciones }}</p>
                </div>
                <div class="modal-footer">
                    <a href="#" v-if="!gestion.PagosGenerados" @click.prevent="generatePagosGestion"   class="btn btn-success"><i class="fas fa-money-bill-wave"></i> Generar Pagos</a>
                    <a href="#" @click.prevent="editGestion"   class="btn btn-warning"><i class="fa fa-edit"></i> {{ trans('labels.actions.edit') }}</a>
                    <a href="#" @click.prevent="deleteGestion" class="btn btn-danger"><i class="fa fa-trash"></i> {{ trans('labels.actions.destroy') }}</a>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script>
    var auth = {!! Auth::user() !!};
    var urlIndexGestion   = '{!! route('Gestion.index')!!}';
    var urlShowGestion    = '{!! route('Gestion.show')!!}';
    var urlSaveGestion    = '{!! route('Gestion.store')!!}';
    var urlDestroyGestion = '{!! route('Gestion.destroy')!!}';
    var urlGeneratePagosGestion = '{!! route('Gestion.generatePagos')!!}';
</script>
{!! Html::script('/js/Gestion/Gestion.js') !!}
@endsection
