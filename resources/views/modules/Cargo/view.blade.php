@extends('layouts.app')
@section('content')
<div id="cargo-app">
    <div class="content">
        <div class="">
            <div class="page-header-title">
                <h4 class="page-title">{{ trans('labels.modules.Cargo') }}</h4>
            </div>
        </div>
        <div class="page-content-wrapper ">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header miClase">
                                <h3 class="card-title text-dark m-0">Registros</h3>
                                <div class="btn-group float-right">
                                    <a href="#" @click.prevent="newCargo" class="btn btn-success waves-effect waves-light m-l-10"><i class="fa fa-plus"></i> {{ trans('labels.actions.new')}}</a>
                                </div>
                            </div>
                            <div class="card-body miClase">
                                <table id="cargo-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- formulario de cargo-->
    <div class="modal fade" id="frm-cargo" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>{{ trans('labels.modules.Cargo') }}</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="Num">Número</label>
                            <input type="text" class="form-control" name="Num" v-model="cargo.Num">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Num"><li class="parsley-required">@{{ errorBag.Num }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="Cargo">Cargo</label>
                            <input type="text" class="form-control" name="Cargo" v-model="cargo.Cargo">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Cargo"><li class="parsley-required">@{{ errorBag.Cargo }}</li></ul>
                        </div>
                        <div class="form-group">
                            <label for="TipoCargo">Tipo de Cargo</label>
                            <input type="text" class="form-control" name="TipoCargo" v-model="cargo.TipoCargo">
                            <ul class="parsley-errors-list filled" id="parsley-id-19" v-if="errorBag.Cargo"><li class="parsley-required">@{{ errorBag.TipoCargo }}</li></ul>
                        </div>
 
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="saveCargo()" class="btn btn-success"><i class="fa fa-save"></i> {{ trans('labels.actions.save') }}</a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-window-close"></i> {{ trans('labels.actions.cancel')}} </button>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
  
     <!-- vista de cargo-->
    <div class="modal fade" id="view-cargo" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title m-0" id="custom-width-modalLabel">{{ trans('labels.modules.Cargo') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>Número</h4>
                    <p class="text-muted">@{{ cargo.Num }}</p>
                    <h4>Cargo</h4>
                    <p class="text-muted">@{{ cargo.Cargo }}</p>
                    <h4>Tipo de Cargo</h4>
                    <p class="text-muted">@{{ cargo.TipoCargo }}</p>
                </div>
                <div class="modal-footer">
                    <a href="#" @click.prevent="editCargo" class="btn btn-warning"><i class="fa fa-edit"></i> {{ trans('labels.actions.edit') }}</a>
                    <a href="#" @click.prevent="deleteCargo" class="btn btn-danger"><i class="fa fa-trash"></i> {{ trans('labels.actions.destroy') }}</a>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script>
    //var auth = {!! Auth::user() !!};
    var urlIndexCargo   = '{!! route('Cargo.index')!!}';
    var urlShowCargo    = '{!! route('Cargo.show')!!}';
    var urlSaveCargo    = '{!! route('Cargo.store')!!}';
    var urlDestroyCargo = '{!! route('Cargo.destroy')!!}';
</script>
{!! Html::script('/js/Cargo/Cargo.js') !!}
@endsection
