<?php

return [
    'modules' => [
        'Inicio' => 'Inicio',
        'Busqueda' => 'Búsqueda',
        'Menu' => 'Menú',
        'Perfil' => 'Mi Perfil',
        'UnidadAcademica' => 'Unidad Académica',
        'Rol' => 'Rol',
        'Roles' => 'Roles',
        'Reparticion' => 'Repartición',
        'Persona' => 'Afiliado',
        'Credencial' => 'Credencial',
        'Parametro' => 'Parámetros',
        'Usuario' => 'Usuarios',
        'NivelAcademico' => 'Nivel Académico',
        'Especialidad' => 'Especialidad|Especialidades',
        'PersonaEspecialidad' => 'Preinscripciones',
        'MisPersonaEspecialidad' => 'Mis solicitudes',

        'Configuracion' => 'Configuración',
        'Roles' => 'Roles de Usuario',
        'Usuarios' => 'Usuarios del Sistema',
        'Usuario' => 'Usuario',
        
        'UnidadAcademicas' => 'Unidades Académicas del Sistema',
        'Reparticiones' => 'Reparticiones del Sistema',
        'Modulos' => 'Módulos',
        'PerfilUsuario' => 'Perfil del Usuario',
        'UsuarioReparticion' => 'Reparticiones del Usuario',

        'ChangePassword' => 'Cambiar Contraseña',
        'Reporte' => 'Reportes',
        'TipoReports' => 'Tipos de Reporte',
        'Help' => 'Ayuda',
        'UserManual' => 'Manual de Usuario',


        'TipoCurso'     => 'Tipos de Curso del Sistema',
        'Curso'         => 'Cursos del Sistema',
        'Requisito'     => 'Requisitos del Sistema',
        'Beca'         => 'Becas del Sistema',


        'Cargo'         => 'Cargos del Sistema',
        'TipoRelacion'         => 'Tipos de Relación del Sistema',
        'Sector'         => 'Sector del Sistema',
        'Gestion'         => 'Gestión|Gestiones',
        'TipoVehiculo'         => 'Tipos de Vehiculos del Sistema',

    ], 
    'actions' => [
        'init' => 'Iniciar',
        'new' => 'Nuevo',
        'add' => 'Añadir',
        'save' => 'Guardar',
        'send' => 'Enviar',
        'edit' => 'Editar',
        'cancel' => 'Cancelar',
        'destroy' => 'Eliminar',
        'do' => 'Realizar',
        'details' => 'Detalles',
        'changepassword' => 'Cambiar Contraseña',
        'reload' => 'Actualizar',
        'results' => 'Resultados',
        'discard' => 'Descartar',
        'print' => 'Imprimir',
        'preview' => 'Vista Previa',
        'validate' => 'Validar',
        'generate' => 'Generar',
        'download' => 'Descargar',
        'confirm'   => 'Confirmar',
        'profile' => 'Perfil',
        'print' => 'Imprimir Filiacion',
        'sect' => 'Imprimir Sector'
    ]
];