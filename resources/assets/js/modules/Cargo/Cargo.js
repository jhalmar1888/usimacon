$(function() {
  
        $("#FechaInicio")
        .datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        })
        .on("change", function() {
          vm.persona.FechaInicio = $("#FechaInicio").val();
        });
    
        $("#FechaFin")
        .datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        })
        .on("change", function() {
          vm.persona.FechaFin = $("#FechaFin").val();
        });
       var cargoTabla = $('#cargo-table').DataTable({
        processing: true,
        order: [[1, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexCargo
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'id', orderable: false, searchable: false , visible: false},
            { data: 'Num', name: 'Num', title: 'Número' },
            { data: 'Cargo', name: 'Cargo', title: 'Cargo' },
            { data: 'TipoCargo', name: 'TipoCargo', title: 'Tipo de Cargo' },            
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column.search(val ? val : '', true, false).draw();
                });                             
            });
        },
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#cargo-table tbody').on('click', 'tr', function () {
        var data = cargoTabla.row( this ).data();
        vm.$options.methods.showCargo(data.id);
    });
});

var vm = new Vue({
    el: '#cargo-app',
    data: {
        //accounting: accounting,
        //auth: auth,
        errorBag: {},
        isLoading: false,
       
        cargo: {},
    },
    methods: {
        ejecutar() {
            alert('Hola' + vm.nombre );
        },
        newCargo () {
            vm.cargo = {};
            $('#frm-cargo').modal('show');
        },
        showCargo (id) {
            axios.post( urlShowCargo, { id: id, cargo: 6 })
                .then ( result => {
                    response = result.data;
                    vm.cargo = response.data;
                    $('#view-cargo').modal('show');
                })
                .catch ( error => {
                    console.log( error );
                });
        },
        editCargo () {
            $('#frm-cargo').modal('show');  
            $('#view-cargo').modal('hide');
        },
        saveCargo () {
            axios.post( urlSaveCargo, vm.cargo)
                .then ( result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    //$('#view-cargo').modal('show');
                    $('#frm-cargo').modal('hide');
                    var cargoTabla = $('#cargo-table').DataTable();
                    cargoTabla.draw();
                })
                .catch( error => {
                    vm.errorBag = error.data.errors;
                });
        },
        deleteCargo () {

            swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.post( urlDestroyCargo, {id : vm.cargo.id} )
                        .then( result => {
                            response = result.data;
                            toastr.success(response.msg, 'Correcto!');
                            var cargoTabla = $('#cargo-table').DataTable();
                            cargoTabla.draw();
                            $('#view-cargo').modal('hide');
                        })
                        .catch( error => {
                            console.log ( error );
                        })
                } else {
                  //swal("Your imaginary file is safe!");
                }
              });
        }
    },
});