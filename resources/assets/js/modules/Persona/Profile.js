$(function () {
    $("#FechaInicioa")
    .datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      language: 'es'
    })
    .on("change", function() {
        vm.personaSector.FechaInicio = $("#FechaInicioa").val();
    });
    
    $("#FechaFina")
    .datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        language: 'es'
    })
    .on("change", function() {
        vm.personaSector.FechaFin = $("#FechaFina").val();
    });
    $("#FechaInicio")
    .datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        language: 'es'
    })
    .on("change", function() {
        vm.personaCargo.FechaInicio = $("#FechaInicio").val();
    });
    
    $("#FechaFin")
    .datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        language: 'es'
    })
    .on("change", function() {
        vm.personaCargo.FechaFin = $("#FechaFin").val();
    });
});

var vm = new Vue({
    el: '#persona-app',
    data: {
        moment: moment,
        accounting: accounting,
        auth: auth,
        errorBag: {},
        isLoading: false,
        isLoadingFile: false,
        isEditing: false,
        persona: persona,
        fotografiamovi: null,

        tipoRelacions: {},
        personaRelacions: {},
        personaRelacion: {},
        cargos: {},
        personaCargos: {},
        personaCargo: {},
        sectors: {},
        personaSectors: {},
        personaSector: {},
        personaDocumentos: {},
        personaDocumento: {},

        personaMovilidades: {},
        personaMovilidad: {},
        radicatorias: radicatorias,
        gestions: {},
        gestion: null,
        pagado: null,
        condonado: null,
        personaPagos: {},
        personaPago: {},
        tipoVehiculos: {},

    },
    methods: {
        uploadFile () {
            vm.isLoadingFile = true;
            var input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = (e) => {
                    this.fotografia = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
                var data = new FormData();
                data.append('File', input.files[0]);
                axios.post(urlUploadFile, data)
                    .then( result => {
                        if (result.data.success) {
                            toastr.info(result.data.msg, 'Correcto!');
                            vm.personaDocumento.Archivo = result.data.data;
                            vm.savePersonaDocumento();
                        } else {
                            toastr.error(result.data.msg, 'Oops!');
                        }
                        vm.isLoadingFile = false;
                    })
                    .catch( error => {
                        vm.isLoadingFile = false;
                        toastr.error('Error subiendo archivo', 'Oops!');
                    });
            }
        },
        getGestion() {
            axios.get( urlListGestion )
                .then( result => {vm.gestions = result.data.data})
                .catch( error => console.log( error ));
        },
        getTipoRelacion () {
            axios.get( urlListTipoRelacion )
                .then( result => {vm.tipoRelacions = result.data.data})
                .catch( error => console.log( error ));
        },
        getCargo () {
            axios.get( urlListCargo )
                .then( result => {vm.cargos = result.data.data})
                .catch( error => console.log( error ));
        },
        getTipoVehiculo () {
            axios.get( urlListTipoVehiculo )
                .then( result => {
                    vm.tipoVehiculos = result.data.data;
                })
                .catch( error => {
                    console.log( error );
                });
        },
        getPersona () {
            axios.get( urlShowPersona, {params: {id: this.persona}} )
                .then( result => {
                    response = result.data;
                    vm.persona = response.data; 
                    vm.getPersonaRelacion();
                })
                .catch( error => {
                    console.log( error );
                })
        },
        async getPersonaRelacion () {
             await axios.get( urlListPersonaRelacion, { params: { Persona: this.persona.id }})
                .then( result => {
                    response = result.data;
                    this.personaRelacions = response.data;
                })
                .catch( error => {
                    console.log( error );
                });
        },
        uploadImageMovi () {
            vm.isLoadingFile = true;
            var input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = (e) => {
                    this.fotografiamovi = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
                var data = new FormData();
                data.append('File', input.files[0]);
                axios.post(urlUploadFile, data)
                    .then( result => {
                        if (result.data.success) {
                            toastr.info(result.data.msg, 'Correcto!');
                            vm.personaMovilidad.Fotografiamovi = result.data.data;
                            
                            //vm.savePersonaMovilidad();
                        } else {
                            toastr.error(result.data.msg, 'Oops!');
                        }
                        vm.isLoadingFile = false;
                    })
                    .catch( error => {
                        vm.isLoadingFile = false;
                        toastr.error('Error subiendo archivo', 'Oops!');
                    });
            }
        },
        newPersonaRelacion() {
            vm.personaRelacion = { Persona: vm.persona.id };
            $('#frm-personaRelacion').modal('show');
        },
        editPersonaRelacion(pr) {
            vm.personaRelacion = pr;
            $('#frm-personaRelacion').modal('show');
        },
        savePersonaRelacion() {
            axios.post(urlSavePersonaRelacion, vm.personaRelacion)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-personaRelacion').modal('hide');
                    vm.getPersonaRelacion(response.data.Persona);
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },        
        deletePersonaRelacion(pr) {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaRelacion, { id: pr.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaRelacion(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        getPersonaDocumento () {
             axios.get( urlListPersonaDocumento, { params: { Persona: this.persona.id }})
                .then( result => {
                    response = result.data;
                    this.personaDocumentos = response.data;
                })
                .catch( error => {
                    console.log( error );
                });
        },
        newPersonaDocumento() {
            vm.personaDocumento = { Persona: vm.persona.id };
            $('#frm-personaDocumento').modal('show');
        },
        editPersonaDocumento(pd) {
            vm.personaDocumento = pd;
            $('#frm-personaDocumento').modal('show');
        },
        savePersonaDocumento() {
            axios.post(urlSavePersonaDocumento, vm.personaDocumento)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-personaDocumento').modal('hide');
                    vm.getPersonaDocumento(response.data.Persona);
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },        
        deletePersonaDocumento(pd) {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaDocumento, { id: pd.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaDocumento(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        downloadPersonaDocumento(pd) {
            axios.get( urlDownloadPersonaDocumento, { params: {id: pd.id }} )
            .then( result => { 
                response = result.data;
                var urlFile = response.data;
                // window.open(urlFile);
                var x = new XMLHttpRequest();
                x.open("GET", urlFile, true);
                x.responseType = 'blob';
                    x.onload = e => {
                        console.log(pd);
                        //vm.isLoading = false; 
                        download(x.response, pd.PersonaDocumento + '.png', "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
        getPersonaMovilidad () {
            axios.get( urlListPersonaMovilidad, { params: { Persona: this.persona.id }})
               .then( result => {
                   response = result.data;
                   this.personaMovilidades = response.data;
                })
               .catch( error => {
                   console.log( error );
               });
        },
        newPersonaMovilidad() {
            vm.personaMovilidad = { Persona: vm.persona.id };
            vm.personaMovilidad.Radicatorias = [];
            $('#frm-personaMovilidad').modal('show');
        },
        editPersonaMovilidad(pm) {
            vm.personaMovilidad = pm;
            $('#frm-personaMovilidad').modal('show');
        },
        savePersonaMovilidad() {
            axios.post(urlSavePersonaMovilidad, vm.personaMovilidad)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-personaMovilidad').modal('hide');
                    vm.getPersonaMovilidad(response.data.Persona);
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },        
        deletePersonaMovilidad(pm) {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaMovilidad, { id: pm.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaMovilidad(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        activarPersonaMovilidad(pm) {
            swal({
                title: "Estas seguro que deseas cambiar el estado del registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlActivarPersonaMovilidad, { id: pm.id, Revisado: !pm.Revisado })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaMovilidad(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        getPersonaCargo () {
            axios.get( urlListPersonaCargo, { params: { Persona: this.persona.id }})
               .then( result => {
                   response = result.data;
                   this.personaCargos = response.data;
               })
               .catch( error => {
                   console.log( error );
               });
        },
        newPersonaCargo() {
            vm.personaCargo = { Persona: vm.persona.id };
            $('#frm-personaCargo').modal('show');
        },
        editPersonaCargo(pc) {
            vm.personaCargo = pc;
            $('#frm-personaCargo').modal('show');
        },
        savePersonaCargo() {
            axios.post(urlSavePersonaCargo, vm.personaCargo)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-personaCargo').modal('hide');
                    vm.getPersonaCargo(response.data.Persona);
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },        
        deletePersonaCargo(pc) {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaCargo, { id: pc.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaCargo(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        
        getSector () {
            axios.get( urlListSector )
                .then( result => {vm.sectors = result.data.data})
                .catch( error => console.log( error ));
        },
        getPersona () {
            axios.get( urlShowPersona, {params: {id: this.persona}} )
                .then( result => {
                    response = result.data;
                    vm.persona = response.data; 
                    //vm.getPersonaSector();
                    vm.getPersonaRelacion();
                })
                .catch( error => {
                    console.log( error );
                })
        },
        getPersonaSector () {
            axios.get( urlListPersonaSector, { params: { Persona: this.persona.id }})
            .then( result => {
                response = result.data;
                this.personaSectors = response.data;
            })
            .catch( error => {
                console.log( error );
            });
        },
        newPersonaSector() {
            vm.personaSector = { Persona: vm.persona.id };
            $('#frm-personaSector').modal('show');
        },
        editPersonaSector(ps) {
            vm.personaSector = ps;
            $('#frm-personaSector').modal('show');
        },
        savePersonaSector() {
            axios.post(urlSavePersonaSector, vm.personaSector)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-personaSector').modal('hide');
                    vm.getPersonaSector(response.data.Persona);
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },        
        deletePersonaSector(ps) {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaSector, { id: ps.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaSector(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        imprimirRegistro() {
            axios.get( urlImprimirRegistro, { params: {id: vm.persona.id }} )
                .then( result => { 
                    response = result.data;
                    var urlFile = response.data.url;
                    // window.open(urlFile);
                    var x = new XMLHttpRequest();
                    x.open("GET", urlFile, true);
                    x.responseType = 'blob';
                    x.onload = e => {
                        //vm.isLoading = false; 
                        download(x.response, vm.persona.Nombre + ' ' + vm.persona.ApPaterno , "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
        imprimirSector() {
            axios.get( urlImprimirSector, { params: {SectorActual: vm.persona.SectorActual }} )
                .then( result => { 
                    response = result.data;
                    var urlFile = response.data.url;
                    // window.open(urlFile);
                    var x = new XMLHttpRequest();
                    x.open("GET", urlFile, true);
                    x.responseType = 'blob';
                    x.onload = e => {
                        //vm.isLoading = false; 
                        download(x.response, vm.persona.Nombre + ' ' + vm.persona.Paterno , "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
        getPersonaPago() {
            axios.get( urlListPersonaPago, { params: { Persona: vm.persona.id, Gestion: vm.gestion, Pagado: vm.pagado, Condonado: vm.condonado}})
                .then( result => {
                    response = result.data;
                    vm.personaPagos = response.data;
                })
                .catch( error => {
                    console.log( error )
                });
        },
        pagarPago(personaPago) {
            swal({
                title: "Estas seguro que deseas realizar el Pago?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlPagarPersonaPago, { id: personaPago.id, Pagado: true })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaPago();
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        condonarPago(personaPago) {
            swal({
                title: "Estas seguro que deseas realizar la Condonación?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlCondonarPersonaPago, { id: personaPago.id, Condonado: true })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaPago();
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        imprimirPago(pp) {
            axios.get( urlImprimirPersonaPago, { params: {id: pp.id }} )
            .then( result => { 
                response = result.data;
                var urlFile = response.url;
                // window.open(urlFile);
                var x = new XMLHttpRequest();
                x.open("GET", urlFile, true);
                x.responseType = 'blob';
                    x.onload = e => {
                        download(x.response, 'Recibo ' + pp.NumeroRecibo + '.pdf', "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
        newPersonaPago() {
            vm.personaPago = { Persona: vm.persona.id };
            $('#frm-personaPago').modal('show');
        },
        editPersonaPago(pp) {
            vm.personaPago = pp;
            vm.personaPago.Gestion = pp.idGestion;
            $('#frm-personaPago').modal('show');
        },
        savePersonaPago() {
            axios.post(urlSavePersonaPago, vm.personaPago)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-personaPago').modal('hide');
                    vm.getPersonaPago(response.data.Persona);
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },        
        deletePersonaPago(pp) {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaPago, { id: pp.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                vm.getPersonaPago(vm.persona.id);
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
    },

    mounted () {
        this.getPersona();
        this.getTipoRelacion();
        this.getTipoVehiculo();
        this.getCargo();
        this.getSector();
        this.getGestion();
        this.getPersonaRelacion();
    } 
});