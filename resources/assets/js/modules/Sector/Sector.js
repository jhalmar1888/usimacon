$(function() {
    var SectorTabla = $('#sector-table').DataTable({
        processing: true,
        order: [[1, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexSector
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'id', orderable: false, searchable: false , visible: false},
            { data: 'Num', name: 'Num', title: 'Número' },
            { data: 'Sector', name: 'Sector', title: 'Sector' },
            { data: 'ZonaSector', name: 'ZonaSector', title: 'Zona Sector' },            
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column.search(val ? val : '', true, false).draw();
                });                             
            });
        },
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#sector-table tbody').on('click', 'tr', function () {
        var data = SectorTabla.row( this ).data();
        vm.$options.methods.showSector(data.id);
    });
});

var vm = new Vue({
    el: '#sector-app',
    data: {
        //accounting: accounting,
        //auth: auth,
        errorBag: {},
        isLoading: false,
       
        sector: {},
    },
    methods: {
        newSector () {
            vm.sector = {};
            $('#frm-sector').modal('show');
        },
        showSector (id) {
            axios.post( urlShowSector, { id: id })
                .then ( result => {
                    response = result.data;
                    vm.sector = response.data;
                    $('#view-sector').modal('show');
                })
                .catch ( error => {
                    console.log( error );
                });
        },
        editSector () {
            $('#frm-sector').modal('show');  
            $('#view-sector').modal('hide');
        },
        saveSector () {
            axios.post( urlSaveSector, vm.sector)
                .then ( result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    //$('#view-sector').modal('show');
                    $('#frm-sector').modal('hide');
                    var SectorTabla = $('#sector-table').DataTable();
                    SectorTabla.draw();
                })
                .catch( error => {
                    vm.errorBag = error.data.errors;
                });
        },
        deleteSector () {

            swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.post( urlDestroySector, {id : vm.sector.id} )
                        .then( result => {
                            response = result.data;
                            toastr.success(response.msg, 'Correcto!');
                            var SectorTabla = $('#sector-table').DataTable();
                            SectorTabla.draw();
                            $('#view-sector').modal('hide');
                        })
                        .catch( error => {
                            console.log ( error );
                        })
                } else {
                  //swal("Your imaginary file is safe!");
                }
              });
        },
        imprimirSector() {
            axios.get( urlImprimirSector, { params: {id: vm.sector.id }} )
                .then( result => { 
                    response = result.data;
                    var urlFile = response.data.url;
                    // window.open(urlFile);
                    var x = new XMLHttpRequest();
                    x.open("GET", urlFile, true);
                    x.responseType = 'blob';
                    x.onload = e => {
                        //vm.isLoading = false; 
                        download(x.response, vm.sector.Sector , "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
    

    },
});