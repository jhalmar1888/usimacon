$(function () {
    var personaMovilidadTabla = $('#personaMovilidad-table').DataTable({
        processing: true,
        order: [[0, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexPersonaMovilidad
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'pm.id', orderable: false, searchable: false, visible: false },
            { data: 'Sector', name: 's.Sector', title: 'Sector', orderable: true, searchable: true },
            { data: 'Persona', name: 'p.Persona', title: 'Nombre Completo', orderable: true, searchable: true },
            { data: 'CI', name: 'p.CI', title: 'C.I.', orderable: true, searchable: true },
            { data: 'TipoVehiculo', name: 'tv.TipoVehiculo', title: 'TipoVehiculo', orderable: true, searchable: true },
            // { data: 'Marca', name: 'Marca', title: 'Marca', orderable: true, searchable: true },
            // { data: 'Beca', name: 'e.Beca', title: 'Beca', orderable: true, searchable: true },
            // { data: 'Capacidad', name: 'Capacidad', title: 'Capacidad', orderable: true, searchable: true },
            // { data: 'Modelo', name: 'Modelo', title: 'Modelo', orderable: true, searchable: true },
            { data: 'Placa', name: 'pm.Placa', title: 'Placa', orderable: true, searchable: true},
            { data: 'Activo', name: 'pm.Activo', title: 'Activo', orderable: false, searchable: false, render: function(data, type, row) { if(row.Activo) return 'Sí'; else return 'No';}},
           
            // { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#personaMovilidad-table tbody').on('click', 'tr', function () {
        var data = personaMovilidadTabla.row(this).data();
        vm.$options.methods.showPersonaMovilidad(data.id);
    });
});

var vm = new Vue({
    el: '#personaMovilidad-app',
    data: {
        moment: moment,
        auth: auth,
        errorBag: {},
        isLoading: false,
        isEditing: false,
        fotografiamovi: null,
        personaEspecialidad: {},
        estados: {},
        Radicatorias: Radicatorias,
        observaciones: null,
        isLoadingFile: false

    },
    methods: {
        getEstados() {
            axios.get( urlListEstado )
                .then( result => { vm.estados = result.data.data})
                .catch( error => { console.log( error )});
        },
        drawEstado(estado) {
            if(estado.Estado == 1)
                return `<span class="badge badge-secondary">${estado.estado.Estado}</span>` ;
            if(estado.Estado == 2)
                return `<span class="badge badge-warning">${estado.estado.Estado}</span>` ;
            if(estado.Estado == 3)
                return `<span class="badge badge-primary">${estado.estado.Estado}</span>` ;
            if(estado.Estado == 4)
                return `<span class="badge badge-secondary">${estado.estado.Estado}</span>` ;
            if(estado.Estado == 5)
                return `<span class="badge badge-dark">${estado.estado.Estado}</span>` ;
            if(estado.Estado == 6)
                return `<span class="badge badge-success">${estado.estado.Estado}</span>` ;
            if(estado.Estado == 7)
                return `<span class="badge badge-danger">${estado.estado.Estado}</span>` ;

            return '';
        },
        newPersonaEspecialidad() {
            vm.personaEspecialidad = {};
            $('#frm-personaEspecialidad').modal('show');
        },
        showPersonaEspecialidad(id) {
            axios.get(urlShowPersonaEspecialidad, { params: { id: id }})
                .then(result => {
                    response = result.data;
                    vm.personaEspecialidad = response.data;
                    $('#view-personaEspecialidad').modal('show');
                })
                .catch(error => {
                    console.log(error);
                });
        },
        savePersonaEspecialidadRequisito(requisito) {
            axios.post(urlSaveRequisito,  requisito)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                })
                .catch(error => {
                    console.log(error);
                    toastr.error(response.msg, 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },
        deletePersonaEspecialidad() {
            swal({
                title: "Estas seguro que deseas eliminar el registro?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersonaEspecialidad, { id: vm.personaEspecialidad.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                var personaEspecialidadTabla = $('#personaEspecialidad-table').DataTable();
                                personaEspecialidadTabla.draw();
                                $('#frm-personaEspecialidad').modal('hide');
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        savePersonaEspecialidadEstado() {
            axios.post( urlSavePersonaEspecialidadEstado, {id: vm.personaEspecialidad.id, Estado: vm.personaEspecialidad.Estado, Observaciones: vm.observaciones})
                .then( result => {
                    response = result.data;
                    vm.showPersonaEspecialidad(response.data.id);
                    toastr.success(response.msg, 'Correcto!');
                    vm.isEditing = false;
                })
                .catch( error => {
                    console.log( error );
                });
        },
        cancelPersonaEspecialidadEstado() {
            vm.showPersonaEspecialidad(vm.personaEspecialidad.id);
            vm.isEditing = false;
        },
        
    },
    mounted() {
        this.getEstados();
    }
});