
var vm = new Vue({
    el: '#reporte-app',
    data: {
        accounting: accounting,
        auth: auth,
        errorBag: {},
        isLoading: false,
        reportes: reportes,
    },
    methods: {
        generateReporte(reporte) {
            axios.post( urlGenerateReporte, reporte )
                .then( result => { 
                    response = result.data;
                    var urlFile = response.data.url;
                    // window.open(urlFile);
                    var x = new XMLHttpRequest();
                    x.open("GET", urlFile, true);
                    x.responseType = 'blob';
                    x.onload = e => {
                        //vm.isLoading = false; 
                        download(x.response, reporte.Reporte , "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
    

    },
});