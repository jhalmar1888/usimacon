require('./bootstrap');
window.Vue = require('vue');

import vSelect from 'vue-select';
//import * as VueGoogleMaps from "vue2-google-maps";
import { LMap, LTileLayer, LMarker, LTooltip, LIcon } from 'vue2-leaflet';
window.moment = require('moment');
window.moment.locale('es');

window.accounting = require('accounting');

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {
    hideModules: { "code": true,  "image": true, },
  
    // you can override icons too, if desired
    // just keep in mind that you may need custom styles in your application to get everything to align
    //iconOverrides: { "bold": "<i class='your-custom-icon'></i>" },
  
    // // if the image option is not set, images are inserted as base64
    // image: {
    //   uploadURL: "/api/myEndpoint",
    //   dropzoneOptions: {}
    // },
    // limit content height if you wish. If not set, editor size will grow with content.
    //    maxHeight: "500px",
    // set to 'true' this will insert plain text without styling when you paste something into the editor.
    forcePlainTextOnPaste: true
  });
// window.Vue.use(VueGoogleMaps, {
//   load: {
//     key: "AIzaSyBqA6n0oAHfQK4OAC02vNOgt0bvWh1qAS0",
//     libraries: "places" // necessary for places input
//   }
// });

//window.Vue.use(LMap, LTileLayer, LMarker);

axios.interceptors.response.use((response) => {
    return response;
}, function (error) {
    return Promise.reject(error.response);
});

/*componentes adicionales*/
Vue.component('loading', require('./components/Loading.vue'));
Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader.vue'));
Vue.component('grid-loader', require('vue-spinner/src/GridLoader.vue'));
Vue.component('skew-loader', require('vue-spinner/src/SkewLoader.vue'));
Vue.component('moon-loader', require('vue-spinner/src/MoonLoader.vue'));
Vue.component('v-select', vSelect);
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);

/*variables globales*/
import { Expedido } from './store/ExpedidoCI';
import { CategoriaLicencia } from './store/CategoriaLicencia';
import { GrupoSanguineo } from './store/GrupoSanguineo';
import { TipoSocio } from './store/TipoSocio';
import { EstadoCivil } from './store/EstadoCivil';
import { Reporte } from './store/Reporte';
import { Radicatoria } from './store/Radicatoria';


window.expedidos = Expedido;
window.grupoSanguineos = GrupoSanguineo;
window.categoriaLicencias = CategoriaLicencia;
window.tipoSocios = TipoSocio;
window.estadoCivils = EstadoCivil;
window.reportes = Reporte;
window.radicatorias = Radicatoria;