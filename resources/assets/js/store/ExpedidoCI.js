export const Expedido = [
    {id: 'LP', Expedido: 'La Paz'},
    {id: 'CHU', Expedido: 'Chuquisaca'},
    {id: 'SRZ', Expedido: 'Santa Cruz'},
    {id: 'OR', Expedido: 'Oruro'},
    {id: 'BE', Expedido: 'Beni'},
    {id: 'PT', Expedido: 'Potosí'},
    {id: 'PAN', Expedido: 'Pando'},
    {id: 'TJ', Expedido: 'Tarija'},
    {id: 'EXT', Expedido: 'Extranjero'},
]