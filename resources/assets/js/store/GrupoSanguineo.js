export const GrupoSanguineo = [
    {id: 'A RH+', GrupoSanguineo: 'A RH+'},
    {id: 'A RH-', GrupoSanguineo: 'A RH-'},
    {id: 'B RH+', GrupoSanguineo: 'B RH+'},
    {id: 'B RH-', GrupoSanguineo: 'B RH-'},
    {id: 'AB RH+', GrupoSanguineo: 'AB RH+'},
    {id: 'AB RH-', GrupoSanguineo: 'AB RH-'},
    {id: '0 RH+', GrupoSanguineo: 'O RH+'},
    {id: '0 RH-', GrupoSanguineo: 'O RH-'},
]