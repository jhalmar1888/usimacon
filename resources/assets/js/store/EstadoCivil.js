export const EstadoCivil = [
    {id: 'SOLTERO(A)', EstadoCivil: 'SOLTERO(A)'},
    {id: 'CASADO(A)', EstadoCivil: 'CASADO(A)'},
    {id: 'DIVORCIADO(A)', EstadoCivil: 'DIVORCIADO(A)'},
    {id: 'VIUDO(A)', EstadoCivil: 'VIUDO(A)'},
    {id: 'OTRO', EstadoCivil: 'OTRO'},
]