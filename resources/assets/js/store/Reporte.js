export const Reporte = [
    {id: '1', Reporte: 'Reporte Consolidado de Deudas', Tipo: 'jasper', Definicion:'ReporteConsolidadoDeudas.jasper'},
    {id: '2', Reporte: 'Reporte Detallado de Deudas', Tipo: 'jasper', Definicion:'ReporteDetalladoDeudas.jasper'},
    {id: '3', Reporte: 'Reporte Afiliados y Placas', Tipo: 'jasper', Definicion:'ReporteAfiliadoPlaca.jasper'},
    {id: '4', Reporte: 'Reporte Vehiculos por Radicatoria', Tipo: 'jasper', Definicion:'ReporteMovilidadRadicatoria.jasper'},
    {id: '5', Reporte: 'Reporte Activos', Tipo: 'jasper', Definicion:'ReporteActivos.jasper'},
    {id: '6', Reporte: 'Reporte Asalariados', Tipo: 'jasper', Definicion:'ReporteAsalariados.jasper'},
    {id: '7', Reporte: 'Reporte Pasivos', Tipo: 'jasper', Definicion:'ReportePasivos.jasper'},
    {id: '8', Reporte: 'Reporte Decanos', Tipo: 'jasper', Definicion:'ReporteDecanos.jasper'},
    {id: '9', Reporte: 'Reporte por Tipo de Camion', Tipo: 'jasper', Definicion:'ReporteCamion.jasper'},
    {id: '10', Reporte: 'Reporte por Tipo de Camioneta', Tipo: 'jasper', Definicion:'ReporteCamioneta.jasper'},
    {id: '11', Reporte: 'Reporte por Tipo de Chasis Cabinado', Tipo: 'jasper', Definicion:'ReporteChasisC.jasper'},
    {id: '12', Reporte: 'Reporte por Tipo de Mixta', Tipo: 'jasper', Definicion:'ReporteMixta.jasper'},
    {id: '13', Reporte: 'Reporte por Tipo de Retroexcabadora', Tipo: 'jasper', Definicion:'ReporteRetroexcabadora.jasper'},
    {id: '14', Reporte: 'Reporte por Tipo de Taxi', Tipo: 'jasper', Definicion:'ReporteTaxi.jasper'},
    {id: '15', Reporte: 'Reporte por Tipo de Vagoneta', Tipo: 'jasper', Definicion:'ReporteVagoneta.jasper'},
    {id: '16', Reporte: 'Reporte por Tipo de Volqueta', Tipo: 'jasper', Definicion:'ReporteVolqueta.jasper'}
];