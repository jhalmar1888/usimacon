export const TipoSocio = [
    {id: 'ACTIVO', TipoSocio: 'ACTIVO'},
    {id: 'PASIVO', TipoSocio: 'PASIVO'},
    {id: 'ASALARIADO', TipoSocio: 'ASALARIADO'},
    {id: 'DECANO', TipoSocio: 'DECANO'},
    {id: 'FUNDADORES', TipoSocio: 'FUNDADORES'},
]