<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonaPago extends Model
{
    protected $table = 'PersonaPago';

    public function gestion () {
        return $this->belongsTo(Gestion::class, 'Gestion');

    }

    public function persona () {
        return $this->belongsTo(Persona::class, 'Persona');

    }

    
}
