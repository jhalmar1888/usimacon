<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{
    protected $table = 'Gestion';
    
    protected $fillable = ['CorrelativoRecibo'];
    
    public function generaPagos() {
        /* genera pagos de la gestion para los 12 meses de cada afiliado activo para cad movilidad */

        for($i = 1; $i<=12; $i++) {
           
            $sql = 'insert into "PersonaPago" ("Persona","Gestion", "Mes", "Monto", created_at, updated_at)
                    select p.id,:gestion,:mes,:monto,:fecha,:fecha
                    from "Persona" p
                    where p."Activo"';
            
            \DB::update($sql,['gestion' => $this->id, 'mes' => $i, 'monto' => $this->Monto, 'fecha' => \Carbon\Carbon::now()]);
        }

        $this->PagosGenerados = true;
        $this->save();
    }

}
