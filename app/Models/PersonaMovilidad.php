<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class PersonaMovilidad extends Model
{
    protected $table = 'PersonaMovilidad';

    protected $appends = ['FotografiamoviUrl'];

    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }

    public function tipoVehiculo() {
        return $this->belongsTo(TipoVehiculo::class, 'TipoVehiculo');
    }

    public function getFotografiamoviURLAttribute() {
        if(!$this->Fotografiamovi)
            return url('/') . '/images/default_image_profile.png';
        else
            return url('') . '/documents/' . $this->Fotografiamovi; 
    }
}
    
