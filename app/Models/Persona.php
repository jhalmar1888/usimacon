<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class Persona extends Authenticatable
{
    use Notifiable;

    protected $table = 'Persona';
    
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $appends = ['FotografiaUrl'];

    public function rol() {
        return $this->belongsTo(Rol::class, 'Rol');
    }
    
    public function personaDocumento() {
        return $this->belongsTo(PersonaDocumento::class, 'Persona');
    }
    
    public function personaSector() {
        return $this->belongsTo(PersonaSector::class, 'Persona');
    }
    
    public function personaCargo() {
        return $this->hasMany(PersonaCargo::class, 'Persona');
    }
    
    public function personaRelacion() { 
        return $this->hasMany(PersonaRelacion::class, 'Persona');
    }
    
    public function personaMovilidad() {
        return $this->hasMany(PersonaMovilidad::class, 'Persona');
    }
    
    public function sectorActual() {
        return $this->belongsTo(Sector::class, 'SectorActual');
    }
    
    public function personaPago() {
        return $this->hasMany(PersonaPago::class, 'Persona');
    }

    public function getFotografiaURLAttribute() {
        if(!$this->Fotografia)
            return url('/') . '/images/default_image_profile.png';
        else
            return url('') . '/documents/' . $this->Fotografia; 
    }

    public function imprimeKardex () {
        $basePathJRXML = storage_path('jrxml');
        $basePathGenerated = public_path('tmp/');

        $fileName = md5($this->CI . Carbon::now());
        
        $basePathJasper = $basePathJRXML . '/Registro.jasper';
        $basePathGenerated = $basePathGenerated . $fileName;

        if(\Storage::exists($basePathGenerated))
            \Storage::delete($basePathGenerated);

        $parametros = array (
            'idPersona' => $this->id,
            'urlLogo' => config('parameters.url_logo')
        );
        
        $database = \Config::get('database.connections.pgsql');
        $database['driver'] = 'postgres';
        
        $reporteJasper = \JasperPHP::process(
            $basePathJasper,
            $basePathGenerated,
            array("pdf"),
            $parametros,
            $database
        );
        //dd($reporteJasper->output());
        $reporteJasper->execute();

        return array(
            'url' => config('parameters.app_url') . '/tmp/' . $fileName . '.pdf',
            'uri' => $basePathGenerated . '.pdf'
        );
    }

    public function registraRequisitos () {
        $requisitos = config('parameters.requisitos');
        //dd($requisitos);
        foreach($requisitos as $r) {
            $personaDocumento = new PersonaDocumento();
            $personaDocumento->Persona = $this->id;
            $personaDocumento->PersonaDocumento = $r;
            $personaDocumento->Presentado = false;
            $personaDocumento->save();
        }
    }
}
