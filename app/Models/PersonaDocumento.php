<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonaDocumento extends Model
{
    protected $table = 'PersonaDocumento';

    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }

}
