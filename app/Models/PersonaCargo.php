<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonaCargo extends Model
{
    protected $table = 'PersonaCargo';

    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }

    public function cargo() {
        return $this->belongsTo(Cargo::class, 'Cargo');
    }
    
}
