<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonaRelacion extends Model
{
    protected $table = 'PersonaRelacion';

    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }

    public function tipoRelacion() {
        return $this->belongsTo(TipoRelacion::class, 'TipoRelacion');
    }
}
