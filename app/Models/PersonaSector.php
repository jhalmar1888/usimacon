<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonaSector extends Model
{
    protected $table = 'PersonaSector';

    public function persona() {
        return $this->belongsTo(Persona::class, 'Persona');
    }

    public function sector() {
        return $this->belongsTo(Sector::class, 'Sector');
    }
}
