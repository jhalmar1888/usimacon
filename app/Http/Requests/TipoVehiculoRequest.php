<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoVehiculoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Num' => 'required|numeric',
            'TipoVehiculo' => 'required',
        ];
    }

    public function messages() {
         return [
              'Num.required' => 'Este campo es requerido.',
              'Num.numeric' => 'El campo debe ser un número..',
              'TipoVehiculo.required' => 'Este campo es requerido.',
         ];
    }
}
