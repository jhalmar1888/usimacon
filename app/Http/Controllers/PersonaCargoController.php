<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\PersonaCargoRequest;
use App\Models\Persona;
use App\Models\PersonaCargo;

class PersonaCargoController extends Controller
{
    public function list(Request $request) {
        $item = PersonaCargo::where('Persona', $request->Persona)->orderBy('id', 'asc')->with('cargo')->get(); 
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function store(PersonaCargoRequest $request) {
        if ($request->id) {
            $item = PersonaCargo::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new PersonaCargo();
            
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Persona = $request->Persona;
        $item->Cargo = $request->Cargo;
        $item->FechaInicio = $request->FechaInicio;
        $item->FechaFin = $request->FechaFin;
        $item->Observaciones = $request->Observaciones;

        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        try {
            PersonaCargo::where('id', $request->id)->delete();
            $success = true;
            $msg = trans('messages.deleted');
        } catch( \Exception $e ) {
            $success = false;
            $msg = trans('messages.error_deleted');
        } finally {
            $result = array(
                'success' => $success,
                'data' => null,
                'msg' => $msg
            );

            return response()->json($result);
        }
    }
}
