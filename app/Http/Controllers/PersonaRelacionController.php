<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\PersonaRelacionRequest;
use App\Models\Persona;
use App\Models\PersonaRelacion;

class PersonaRelacionController extends Controller
{
    public function list(Request $request) {
        $item = PersonaRelacion::where('Persona', $request->Persona)->orderBy('id', 'asc')->with('tipoRelacion')->get(); 
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function store(PersonaRelacionRequest $request) {
        if ($request->id) {
            $item = PersonaRelacion::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new PersonaRelacion();
            
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Persona = $request->Persona;
        $item->PersonaRelacion = $request->PersonaRelacion;
        $item->TipoRelacion = $request->TipoRelacion;

        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        try {
            PersonaRelacion::where('id', $request->id)->delete();
            $success = true;
            $msg = trans('messages.deleted');
        } catch( \Exception $e ) {
            $success = false;
            $msg = trans('messages.error_deleted');
        } finally {
            $result = array(
                'success' => $success,
                'data' => null,
                'msg' => $msg
            );

            return response()->json($result);
        }
    }
}