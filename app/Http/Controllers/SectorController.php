<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\SectorRequest;
use App\Models\Sector;

class SectorController extends Controller
{
	public function view () {
        return view('modules.Sector.view');
    }
    
    public function index () {
        $sector = Sector::select('id', 'Num','Sector','ZonaSector');
        
        return Datatables::of($sector)
            ->addColumn('action', function ($p) {
                return '<a href="#" @click.prevent="showSector('. $p->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> '. trans('labels.actions.details') .'</a> &nbsp;';
            })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function list(Request $request) {
        $item = new Sector();
        $objeto = null;

        $objeto = $item->orderBy('Sector', 'asc')->get();
        
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show (Request $request) {
        
        try {
            $item = Sector::findOrFail($request->id);
            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.listed')
            );
        } catch(\Exception $e) {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => trans('mesagges.error')
            );
        } finally {
            return response()->json($data);
        }
    }

    public function store (SectorRequest $request) {
        if($request->id) {
            $item = Sector::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Sector();
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Usuario;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }
        
        $item->Sector = $request->Sector;
        $item->ZonaSector = $request->ZonaSector;
        $item->Num = $request->Num;
        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        Sector::where('id', $request->id)->delete();
        $result = array (
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );
        
        return response()->json($result);
    }

    public function imprimirSector (Request $request) {
        $sector = Sector::findOrFail($request->id);
        $fileName = md5($sector->Sector . Carbon::now());
        $basePathJasper = storage_path('/jrxml/') . 'Sector.jasper';
        $basePathGenerated = public_path('/tmp/') . $fileName;

        if(\Storage::exists($basePathGenerated))
            \Storage::delete($basePathGenerated);

        $parametros = array(
            'idSector' => $sector->id,
            'urlLogo' => url('/') . '/images/emi_logo.png',
           //0 'pathsubpersonafamiliarJasper' => storage_path('/jrxml/') . 'FiliacionPersonaFamiliar.jasper',
            //'pathsubpersonavehiculoJasper' => storage_path('/jrxml/') . 'FiliacionPersonaVehiculo.jasper'
        );

        $database = config('database.connections.pgsql');
        $database['driver'] = 'postgres';
        //dd($parametros);
        $reporteJasper = \JasperPHP::process(
            $basePathJasper, //que archivo jasper debo procesar?
            $basePathGenerated, //donde debo generar el archivo resultante??
            array('pdf'), //en que formatos debo generar el reporte?
            $parametros, //que parametros debo pasarle al archivo jasper?
            $database //como me conecto a la base de datos
        );
        //dd($reporteJasper->output());
        //return ($reporteJasper->output());
        $reporteJasper->execute();
        //return response()->download($basePathGenerated . '.pdf', 'Historial ' . $persona->Nombre . ' ' . $persona->Paterno .'.pdf');
        $path =  array(
            'url' => url('/') . '/tmp/' . $fileName . '.pdf',
            'uri' => $basePathGenerated . '.pdf'
        );

        $result = array (
            'success' => true,
            'data' => $path,
            'msg' => 'rerporte generado correctamente'
        );
        
        return response()->json($result);
    }
}
