<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\PersonaMovilidad;
use Yajra\Datatables\Datatables;


use App\Models\Persona;
use App\Models\TipoVehiculo;
class PersonaMovilidadController extends Controller
{
    public function view(){
       
        return view('modules.PersonaMovilidad.view');
    }

    public function index() {

        $item = PersonaMovilidad::from('PersonaMovilidad as pm')
                        ->join('Persona as p', 'pm.Persona', '=', 'p.id')
                        ->join('TipoVehiculo as tv', 'pm.TipoVehiculo', '=', 'tv.id')
                        ->leftJoin('Sector as s', 'p.SectorActual', '=', 's.id')
                        ->select('pm.id', 'p.Persona', 'p.CI', 'tv.TipoVehiculo', 'pm.Placa', 'pm.Activo','pm.Revisado','s.Sector'); 
        return Datatables::of($item)
            ->addColumn('action', function ($p) {
                return '<a href="#" @click.prevent="showPersonaMovilidad(' . $p->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> ' . trans('labels.actions.details') . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }
    
    
    
    public function list(Request $request) {
        $item = PersonaMovilidad::with('tipoVehiculo')->orderBy('id', 'asc'); 

        if($request->Persona)
            $item = $item->where('Persona', $request->Persona);

        if($request->Activo)
            $item = $item->where('Activo', $request->Activo);
        $item = $item->get();
        
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function store(Request $request) {

        //dd($request);

        if(PersonaMovilidad::where('Persona',$request->Persona)->where('Activo',$request->Ativo)->count() > 3)
        {
           session()->flash('error','El registro ya existe');

           return Redirect::to(route('Estudiante.show', $request->idAlumno));
        }

        if ($request->id) {
            $item = PersonaMovilidad::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
           

            $item = new PersonaMovilidad();
            
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Persona = $request->Persona;
        $item->TipoVehiculo = $request->TipoVehiculo;
        $item->Marca = $request->Marca;
        $item->Capacidad = $request->Capacidad;
        $item->Modelo = $request->Modelo;
        $item->Placa = $request->Placa;
        $item->Color = $request->Color;
        $item->NumeroChasis = $request->NumeroChasis;
        $item->Observaciones = $request->Observaciones;
        $item->Radicatoria = $request->Radicatoria;
        $item->Fotografiamovi = $request->Fotografiamovi;
        $item->Activo = $request->Activo ? true : false;


        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        try {
            PersonaMovilidad::where('id', $request->id)->delete();
            $success = true;
            $msg = trans('messages.deleted');
        } catch( \Exception $e ) {
            $success = false;
            $msg = trans('messages.error_deleted');
        } finally {
            $result = array(
                'success' => $success,
                'data' => null,
                'msg' => $msg
            );

            return response()->json($result);
        }
    }

    public function activar(Request $request) {
        $item = PersonaMovilidad::find($request->id);

        if(PersonaMovilidad::where('Persona', $item->Persona)->where('Revisado',1)->count() >= 2 && $request->Revisado) {
            $data = array(
                'success' => false,
                'data' => $item,
                'msg' => 'Sólo puede tener activas dos movilidades a la vez'
            );

            return response()->json($data);
        } else {
            $item->Revisado = $request->Revisado;

            $item->UpdaterUserName = \Auth::user()->email;
            $item->UpdaterFullUserName = \Auth::user()->Persona;
            $item->UpdaterIP = $request->ip();
            $item->save();

            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.updated')
            );

            return response()->json($data);
        }



    }
}
