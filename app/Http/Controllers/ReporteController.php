<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use JasperPHP\Facades\JasperPHP;

class ReporteController extends Controller
{
    public function view() {
        return view('modules.Reporte.view');
    }

    public function generate(Request $request) {
        $fileName = md5(Carbon::now());
        $basePathJasper = storage_path('/jrxml/') . $request->Definicion;
        $basePathGenerated = public_path('/tmp/') . $fileName;

        if(\Storage::exists($basePathGenerated))
            \Storage::delete($basePathGenerated);

        $parametros = array(
            'urlLogo' => url('/') . '/images/emi_logo.png',
        );

        $database = config('database.connections.pgsql');
        $database['driver'] = 'postgres';
        //dd($parametros);
        $reporteJasper = \JasperPHP::process(
            $basePathJasper, //que archivo jasper debo procesar?
            $basePathGenerated, //donde debo generar el archivo resultante??
            array('pdf'), //en que formatos debo generar el reporte?
            $parametros, //que parametros debo pasarle al archivo jasper?
            $database //como me conecto a la base de datos
        );
        //dd($reporteJasper->output());
        //return ($reporteJasper->output());
        $reporteJasper->execute();
        //return response()->download($basePathGenerated . '.pdf', 'Historial ' . $persona->Nombre . ' ' . $persona->Paterno .'.pdf');
        $path =  array(
            'url' => config('parameters.app_url') . '/tmp/' . $fileName . '.pdf',
            'uri' => $basePathGenerated . '.pdf'
        );

        $result = array (
            'success' => true,
            'data' => $path,
            'msg' => 'rerporte generado correctamente'
        );
        
        return response()->json($result);
    }
}
