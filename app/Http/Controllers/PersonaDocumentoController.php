<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\PersonaDocumentoRequest;
use App\Models\Persona;
use App\Models\PersonaDocumento;

class PersonaDocumentoController extends Controller
{
    public function list(Request $request) {
        $item = PersonaDocumento::where('Persona', $request->Persona)->orderBy('id', 'asc')->get(); 
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function store(PersonaDocumentoRequest $request) {
        if ($request->id) {
            $item = PersonaDocumento::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new PersonaDocumento();
            
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Persona = $request->Persona;
        $item->PersonaDocumento = $request->PersonaDocumento;
        $item->Archivo = $request->Archivo;
        $item->Presentado = $request->Presentado == 1 ? true : false;
        $item->Observaciones = $request->Observaciones;

        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        try {
            PersonaDocumento::where('id', $request->id)->delete();
            $success = true;
            $msg = trans('messages.deleted');
        } catch( \Exception $e ) {
            $success = false;
            $msg = trans('messages.error_deleted');
        } finally {
            $result = array(
                'success' => $success,
                'data' => null,
                'msg' => $msg
            );

            return response()->json($result);
        }
    }

    public function download (Request $request) {
        $item = PersonaDocumento::find($request->id);

        if($item && $item->Archivo) {
            $url = url('/') . '/documents/' . $item->Archivo;
            $data = array(
                'success' => true,
                'data' => $url,
                'msg' => trans('messages.found')
            );
        } else {
            $data = array(
                'success' => false,
                'data' => $item,
                'msg' => trans('messages.not_found')
            );
        }

        return response()->json($data);
    }
}