<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\GestionRequest;
use App\Models\Gestion;


class GestionController extends Controller
{
    public function view () {
        return view('modules.Gestion.view');
    }
    
    public function index () {
        $gestion = Gestion::select('id', 'Gestion','Monto','Observaciones');
        
        return Datatables::of($gestion)
            ->addColumn('action', function ($g) {
                return '<a href="#" @click.prevent="showSector('. $g->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> '. trans('labels.actions.details') .'</a> &nbsp;';
            })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function list(Request $request) {
        $item = new Gestion();
        $objeto = null;

        $objeto = $item->orderBy('Gestion', 'asc')->get();
        
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show (Request $request) {
        try {
            $item = Gestion::findOrFail($request->id);
            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.listed')
            );
        } catch(\Exception $e) {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => trans('mesagges.error')
            );
        } finally {
            return response()->json($data);
        }
    }

    public function store (GestionRequest $request) {
        if($request->id) {
            $item = Gestion::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Gestion();
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Usuario;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }
        
        $item->Gestion = $request->Gestion;
        $item->Monto = $request->Monto;
        $item->Observaciones = $request->Observaciones;
        $item->Sgeneral = $request->Sgeneral;
        $item->Shacienda = $request->Shacienda;
        $item->Sconflicto = $request->Sconflicto;
        $item->Sregimen = $request->Sregimen;

        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        Gestion::where('id', $request->id)->delete();
        $result = array (
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );
        
        return response()->json($result);
    }

    public function generatePagos(Request $request) {
        $gestion = Gestion::find($request->id);
        $gestion->generaPagos();

        $data = array(
            'success' => true,
            'data' => $gestion,
            'msg' => 'Generado correctamente'
        );

        return response()->json($data);
    }
}
