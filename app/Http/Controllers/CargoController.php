<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\CargoRequest;
use App\Models\Cargo;



class CargoController extends Controller
{
	public function view () {
        return view('modules.Cargo.view');
    }
    
    public function index () {
        $cargo = Cargo::select('id','Num' ,'Cargo','TipoCargo');
        
        return Datatables::of($cargo)
            ->addColumn('action', function ($p) {
                return '<a href="#" @click.prevent="showCargo('. $p->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> '. trans('labels.actions.details') .'</a> &nbsp;';
            })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function list(Request $request) {
        $item = new Cargo();
        $objeto = null;

        $objeto = $item->orderBy('Cargo', 'asc')->get();
        
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show (Request $request) {
        
        try {
            $item = Cargo::findOrFail($request->id);
            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.listed')
            );
        } catch(\Exception $e) {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => trans('mesagges.error')
            );
        } finally {
            return response()->json($data);
        }
    }

    public function store (CargoRequest $request) {
        if($request->id) {
            $item = Cargo::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Cargo();
            // $item->CreatorUserName = \Auth::user()->email;
            // $item->CreatorFullUserName = \Auth::user()->Usuario;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }
        
        $item->Cargo = $request->Cargo;
        $item->TipoCargo = $request->TipoCargo;
        $item->Num = $request->Num;
        // $item->UpdaterUserName = \Auth::user()->email;
        // $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        Cargo::where('id', $request->id)->delete();
        $result = array (
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );
        
        return response()->json($result);
    }
}
