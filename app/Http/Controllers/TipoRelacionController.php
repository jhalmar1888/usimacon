<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\TipoRelacionRequest;
use App\Models\TipoRelacion;

class TipoRelacionController extends Controller
{
	public function view () {
        return view('modules.TipoRelacion.view');
    }
    
    public function index () {
        $tipoRelacion = TipoRelacion::select('id', 'Num', 'TipoRelacion');
        
        return Datatables::of($tipoRelacion)
            ->addColumn('action', function ($p) {
                return '<a href="#" @click.prevent="showTipoRelacion('. $p->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> '. trans('labels.actions.details') .'</a> &nbsp;';
            })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function list(Request $request) {
        $item = new TipoRelacion();
        $objeto = null;

        $objeto = $item->orderBy('TipoRelacion', 'asc')->get();
        
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show (Request $request) {
        
        try {
            $item = TipoRelacion::findOrFail($request->id);
            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.listed')
            );
        } catch(\Exception $e) {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => trans('mesagges.error')
            );
        } finally {
            return response()->json($data);
        }
    }

    public function store (TipoRelacionRequest $request) {
        if($request->id) {
            $item = TipoRelacion::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new TipoRelacion();
            // $item->CreatorUserName = \Auth::user()->email;
            // $item->CreatorFullUserName = \Auth::user()->Usuario;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }
        
        $item->TipoRelacion = $request->TipoRelacion;
        $item->Num = $request->Num;
        // $item->UpdaterUserName = \Auth::user()->email;
        // $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        TipoRelacion::where('id', $request->id)->delete();
        $result = array (
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );
        
        return response()->json($result);
    }
}
