<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\TipoVehiculoRequest;
use App\Models\TipoVehiculo;


class TipoVehiculoController extends Controller
{
	public function view () {
        return view('modules.TipoVehiculo.view');
    }
    
    public function index () {
        $TipoVehiculo = TipoVehiculo::select('id', 'Num', 'TipoVehiculo');
        
        return Datatables::of($TipoVehiculo)
            ->addColumn('action', function ($p) {
                return '<a href="#" @click.prevent="showTipoVehiculo('. $p->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> '. trans('labels.actions.details') .'</a> &nbsp;';
            })
        ->editColumn('id', '{{$id}}')
        ->make(true);
    }

    public function list(Request $request) {
        $item = new TipoVehiculo();
        $objeto = null;

        $objeto = $item->orderBy('TipoVehiculo', 'asc')->get();
        
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show (Request $request) {
        
        try {
            $item = TipoVehiculo::findOrFail($request->id);
            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.listed')
            );
        } catch(\Exception $e) {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => trans('mesagges.error')
            );
        } finally {
            return response()->json($data);
        }
    }

    public function store (TipoVehiculoRequest $request) {
        if($request->id) {
            $item = TipoVehiculo::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new TipoVehiculo();
            // $item->CreatorUserName = \Auth::user()->email;
            // $item->CreatorFullUserName = \Auth::user()->Usuario;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }
        
        $item->TipoVehiculo = $request->TipoVehiculo;
        $item->Num = $request->Num;
        // $item->UpdaterUserName = \Auth::user()->email;
        // $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        TipoVehiculo::where('id', $request->id)->delete();
        $result = array (
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );
        
        return response()->json($result);
    }
}
