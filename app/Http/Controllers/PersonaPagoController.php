<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\PersonaPagoRequest;
use App\Models\PersonaPago;
use App\Models\Gestion;
use Illuminate\Support\Facades\DB;

class PersonaPagoController extends Controller
{
    // public function view () {
    //     return view('modules.Gestion.view');
    // }
    
    // public function index () {
    //     $gestion = Gestion::select('id', 'Gestion','Monto','Observaciones');
        
    //     return Datatables::of($gestion)
    //         ->addColumn('action', function ($g) {
    //             return '<a href="#" @click.prevent="showSector('. $g->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> '. trans('labels.actions.details') .'</a> &nbsp;';
    //         })
    //     ->editColumn('id', '{{$id}}')
    //     ->make(true);
    // }

    public function list(Request $request) {
        $item = PersonaPago::from('PersonaPago as pp')
                    ->join('Gestion as g', 'pp.Gestion', '=', 'g.id');

        /*filtro por persona*/
        if($request->Persona) 
            $item = $item->where('pp.Persona', $request->Persona );
        
        /*filtro por gestion*/
        if($request->Gestion) 
            $item = $item->where('pp.Gestion', $request->Gestion );
        else
            $item = $item->where('g.Gestion', Gestion::max('Gestion') );
        
        /*filtro por pagado*/
        if($request->Pagado) 
            $item = $item->where('pp.Pagado', $request->Pagado );
        
        /*filtro por condonado*/
        if($request->Condonado) 
            $item = $item->where('pp.Condonado', $request->Condonado );
        
        $objeto = $item->select('pp.id', 'pp.Monto', 'g.id as idGestion' ,'g.Gestion','pp.Mes', 'pp.Pagado', 'pp.Condonado','pp.NumeroRecibo', 'pp.Observaciones')
                    ->orderBy('g.Gestion', 'asc')->orderBy('pp.Mes', 'asc')->get();
        
        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show (Request $request) {
        try {
            $item = PersonaPago::where('id', $request->id)->with('gestion')->first();
            $data = array(
                'success' => true,
                'data' => $item,
                'msg' => trans('messages.listed')
            );
        } catch(\Exception $e) {
            $data = array(
                'success' => false,
                'data' => null,
                'msg' => trans('mesagges.error')
            );
        } finally {
            return response()->json($data);
        }
    }

    public function store (PersonaPagoRequest $request) {
        if($request->id) {
            $item = PersonaPago::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new PersonaPago();
            $item->Persona = $request->Persona;
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Usuario;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }
        
        $item->Gestion = $request->Gestion;
        $item->Mes = $request->Mes;
        $item->Monto = $request->Monto;
        $item->Pagado = $request->Pagado == 1 ? true : false;
        $item->Condonado = $request->Condonado == 1 ? true : false;
        $item->Observaciones = $request->Observaciones;

        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    public function destroy(Request $request) {
        PersonaPago::where('id', $request->id)->delete();
        $result = array (
            'success' => true,
            'data' => null,
            'msg' => trans('messages.deleted')
        );
        
        return response()->json($result);
    }

    public function pagar(Request $request) {
        $item = PersonaPago::find($request->id);
        $item->Pagado = $request->Pagado;
        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        /*actualiza numero de recibo*/
        $item->NumeroRecibo = $item->gestion->CorrelativoRecibo + 1;
        $item->FechaPago = Carbon::now();
        $item->gestion->update(['CorrelativoRecibo' => $item->NumeroRecibo]);
        
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.pagado')
        );


        return response()->json($result);

    }
    
    public function condonar(Request $request) {
        $item = PersonaPago::find($request->id);
        $item->Condonado = $request->Condonado;
        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Usuario;
        $item->UpdaterIP = $request->ip();
        $item->save();

        $result = array (
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.condonado')
        );

        return response()->json($result);
    }


    public function imprimir (Request $request) {
        $item = PersonaPago::find($request->id);

        $basePathJRXML = storage_path('jrxml');
        $basePathGenerated = public_path('tmp/');

        $fileName = md5($item->id . Carbon::now());
        
        $basePathJasper = $basePathJRXML . '/Recibo.jasper';
        $basePathGenerated = $basePathGenerated . $fileName;

        if(\Storage::exists($basePathGenerated))
            \Storage::delete($basePathGenerated);

        setlocale(LC_ALL,"es_ES"); 
        // \Carbon\Carbon::setLocale('es'); 
        $fecha = \Carbon\Carbon::parse($item->FechaPago);
        // $fecha = Carbon::createFromFormat('Y-m-d', $item->FechaPago);
        $meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];

        $parametros = array (
            'idPersonaPago' => $item->id,
            'texto' => "He recibido del Sr(a). " . $item->persona->Persona . " la suma de Bs. " .$item->Monto . " BOLIVIANOS. Por concepto de pago de cuota mensual correspondiente al mes de ". $meses[$item->Mes - 1] ." del " . $item->gestion->Gestion . ".",
            'fecha' => "La Paz, " . $fecha->format('d') . " de " . $fecha->formatLocalized('%B') . ' del ' . $fecha->format('Y'),
            'urlLogo' => url('/') . '/images/emi_logo.png'
        );
        
        $database = \Config::get('database.connections.pgsql');
        $database['driver'] = 'postgres';
        
        $reporteJasper = \JasperPHP::process(
            $basePathJasper,
            $basePathGenerated,
            array("pdf"),
            $parametros,
            $database
        );
        //dd($reporteJasper->output());
        $reporteJasper->execute();

        $data = array(
            'url' => config('parameters.app_url') . '/tmp/' . $fileName . '.pdf',
            'uri' => $basePathGenerated . '.pdf'
        );

        return response()->json($data);
    }

    public function gestionPagos(Request $request)
    {
      //  $datos=new PersonaPago()::where('');
        /*$datos = PersonaPago::select("Gestion.Gestion")->join('Gestion', 'PersonaPago.Gestion', '=', 'Gestion.id')
                ->count("Gestion.Gestion")
                ->groupBy('Gestion.Gestion')
                ->get();
                */

        $query= 'SELECT "Gestion"."Gestion", sum("PersonaPago"."Monto") as cantidad
                    FROM "PersonaPago", "Gestion"
                    WHERE "PersonaPago"."Gestion"="Gestion".id and "NumeroRecibo">0 group by  "Gestion"."Gestion"
                     ';
        $object = DB::select(DB::raw($query));


        $result = array (
            'success' => true,
            'data' => $object,
            'msg' => trans('messages.condonado')
        );

        return response()->json($result);

    }

}
