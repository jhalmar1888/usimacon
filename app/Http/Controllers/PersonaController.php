<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Http\Requests\PersonaRequest;
use App\Models\Persona;
use App\Models\PersonaEspecialidad;
use App\Models\PersonaDocumento;
use App\Models\PersonaRelacion;
use App\Models\PersonaSector;


class PersonaController extends Controller
{
    public function view(){
       
        return view('modules.Persona.view');
    }

    public function index() {

        $item = Persona::from('Persona as p')
                        ->leftjoin('Rol as r', 'p.Rol', '=', 'r.id')
                        ->leftjoin('PersonaMovilidad as pm', 'p.id', '=', 'pm.Persona')
                        ->select('p.id', 'p.CI', 'p.Persona', 'p.FechaNacimiento', 'p.Sexo', 'p.Direccion', 'p.Telefono', 'p.Celular', 'p.email', 'r.Rol', 'pm.Placa','p.TipoSocio','p.Activo'); 
        return Datatables::of($item)
            ->addColumn('action', function ($p) {
                return '<a href="#" @click.prevent="showPersona(' . $p->id . ')" class="btn btn-info btn-xs"><i class="fa fa-bars"></i> ' . trans('labels.actions.details') . '</a> &nbsp;';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }

    public function list(Request $request) {
   
        $item = new Persona();
        $objeto = null;

        $objeto = $item->orderBy('id', 'asc')->get();

        $data = array(
            'success' => true,
            'data' => $objeto,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function show(Request $request) {
        $item = Persona::where('id', $request->id)->with('sectorActual')->first();
            // ->with('unidadAcademica', 'rol','personaEspecialidad.especialidad','personaEspecialidad.estado','personaEspecialidad.personaEspecialidadRequisito.requisito')->first();
        $data = array(
            'success' => true,
            'data' => $item,
            'msg' => trans('messages.listed')
        );

        return response()->json($data);
    }

    public function store(PersonaRequest $request) {
        $nuevo = false;
        if ($request->id) {
            $item = Persona::findOrFail($request->id);
            $msg = trans('messages.updated');
        } else {
            $item = new Persona();
            $nuevo = true;            
            $item->CreatorUserName = \Auth::user()->email;
            $item->CreatorFullUserName = \Auth::user()->Persona;
            $item->CreatorIP = $request->ip();
            $msg = trans('messages.added');
        }

        $item->Rol = $request->Rol;
        $item->Nombre = $request->Nombre;
        $item->ApPaterno = $request->ApPaterno;
        $item->ApMaterno = $request->ApMaterno;
        $item->Persona = $item->ApPaterno . ' ' . $item->ApMaterno . ' ' . $item->Nombre;
        
        $item->CI = $request->CI;
        $item->Expedido = $request->Expedido;
        $item->FechaNacimiento = $request->FechaNacimiento;
        $item->Sexo = $request->Sexo;
        $item->EstadoCivil = $request->EstadoCivil;
        $item->SectorActual = $request->SectorActual;
        $item->TipoSocio = $request->TipoSocio;
        $item->Direccion = $request->Direccion;
        $item->Telefono = $request->Telefono;
        $item->Celular = $request->Celular;
        $item->GrupoSanguineo = $request->GrupoSanguineo;
        if($request->CategoriaLicencia) $item->CategoriaLicencia = implode(',', $request->CategoriaLicencia);
        $item->Observaciones = $request->Observaciones;
        $item->Activo = $request->Activo == 1 ? true : false;
        $item->email = $request->email;
        $item->FechaIngreso = $request->FechaIngreso;
        $item->Fotografia = $request->Fotografia;
        $item->Antecedente = $request->Antecedente;
        
        if($request->password) $item->password = bcrypt($request->password);

        $item->UpdaterUserName = \Auth::user()->email;
        $item->UpdaterFullUserName = \Auth::user()->Persona;
        $item->UpdaterIP = $request->ip();
        $item->save();

        if($nuevo) 
            $item->registraRequisitos();

        $result = array(
            'success' => true,
            'data' => $item,
            'msg' => $msg
        );
        return response()->json($result);
    }

    //  public function destroy(Request $request) {
    //      try {
        
    //         Persona::where('id', $request->id)->delete();

           
    //         $success = true;
    //         $msg = trans('messages.deleted');


           

    //     } catch( \Exception $e ) {
    //         $success = false;
    //         $msg = trans('messages.error_deleted');
    //     } finally {
    //         $result = array(
    //             'success' => $success,
    //             'data' => null,
    //             'msg' => $msg
    //         );

    //         return response()->json($result);
    //     }
    // } 

    public function destroy(Request $request) {

        $perdoc = PersonaDocumento::where('Persona', $request->id)->get();
        $perrel = PersonaRelacion::where('Persona', $request->id)->get();
        $persec = PersonaSector::where('Persona', $request->id)->get();
        //dd($perdoc);
        //dd($perrel);
        foreach ($perdoc as $value) {
            // echo "$value <br>";
            PersonaDocumento::where('id', $value->id)->delete();

          }
          foreach ($perrel as $value) {
            // echo "$value <br>";
            PersonaRelacion::where('id', $value->id)->delete();

          }
          foreach ($persec as $value) {
            // echo "$value <br>";
            PersonaSector::where('id', $value->id)->delete();

          }

        // echo gettype($perdoc);
        //($perdoc);

        $var = Persona::where('id', $request->id)->delete();

        //dd($var);

        // $success = true;
        $msg = trans('messages.deleted');


        $result = array(
            'success' => true,
            'data' => null,
            'msg' => $msg
        );

        return response()->json($result);
    } 

    public function profile(Request $request) {
        $persona = $request->id;
        return view('modules.Persona.profile', ['persona' => $persona]);
    }
    
    public function imprimirRegistro (Request $request) {
        $persona = Persona::findOrFail($request->id);

        $fileName = md5($persona->CI . Carbon::now());
        $basePathJasper = storage_path('/jrxml/') . 'Filiacion.jasper';
        $basePathGenerated = public_path('/tmp/') . $fileName;

        if(\Storage::exists($basePathGenerated))
            \Storage::delete($basePathGenerated);

        $parametros = array(
            'idPersona' => $persona->id,
            'urlLogo' => url('/') . '/images/emi_logo.png',
            'pathsubpersonafamiliarJasper' => storage_path('/jrxml/') . 'FiliacionPersonaFamiliar.jasper',
            'pathsubreportpersonasectorJasper' => storage_path('/jrxml/') . 'FiliacionPersonaSector.jasper',
            'pathsubpersonacargoJasper' => storage_path('/jrxml/') . 'FiliacionPersonaCargo.jasper',
            'pathsubreportpersonapagoJasper' => storage_path('/jrxml/') . 'FiliacionPersonaPago.jasper',
            'pathsubpersonavehiculoJasper' => storage_path('/jrxml/') . 'FiliacionPersonaVehiculo.jasper',
            'urlFoto' => $persona->FotografiaUrl
        );

        $database = config('database.connections.pgsql');
        $database['driver'] = 'postgres';
        //dd($parametros);
        $reporteJasper = \JasperPHP::process(
            $basePathJasper, //que archivo jasper debo procesar?
            $basePathGenerated, //donde debo generar el archivo resultante??
            array('pdf', 'xls'), //en que formatos debo generar el reporte?
            $parametros, //que parametros debo pasarle al archivo jasper?
            $database //como me conecto a la base de datos
        );
        //dd($reporteJasper->output());
        //return ($reporteJasper->output());
        $reporteJasper->execute();
        //return response()->download($basePathGenerated . '.pdf', 'Historial ' . $persona->Nombre . ' ' . $persona->Paterno .'.pdf');
        $path =  array(
            'url' => config('parameters.app_url') . '/tmp/' . $fileName . '.pdf',
            'uri' => $basePathGenerated . '.pdf'
        );

        $result = array (
            'success' => true,
            'data' => $path,
            'msg' => 'rerporte generado correctamente'
        );
        
        return response()->json($result);
    }


}
