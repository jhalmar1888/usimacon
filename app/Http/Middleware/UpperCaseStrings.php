<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class UpperCaseStrings extends TransformsRequest
{
     /**
     * The attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        '_token',
        'password',
        'email',
        'Imagen',
        'Archivo',
        'Foto',
        'Reporte',
        'Definicion',
    ];

    /**
     * Transform the given value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function transform($key, $value)
    {
        if (in_array($key, $this->except, true)) {
            return $value;
        }

        return is_string($value) ? strtoupper($value) : $value;
    }
}
