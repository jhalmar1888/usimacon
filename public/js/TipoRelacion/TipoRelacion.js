$(function() {
    var tipoRelacionTabla = $('#tipoRelacion-table').DataTable({
        processing: true,
        order: [[1, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexTipoRelacion
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'id', orderable: false, searchable: false , visible: false},
            { data: 'Num', name: 'Num', title: 'Número' },
            { data: 'TipoRelacion', name: 'TipoRelacion', title: 'TipoRelacion' },            
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column.search(val ? val : '', true, false).draw();
                });                             
            });
        },
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#tipoRelacion-table tbody').on('click', 'tr', function () {
        var data = tipoRelacionTabla.row( this ).data();
        vm.$options.methods.showTipoRelacion(data.id);
    });
});

var vm = new Vue({
    el: '#tipoRelacion-app',
    data: {
        //accounting: accounting,
        //auth: auth,
        errorBag: {},
        isLoading: false,
       
        tipoRelacion: {},
    },
    methods: {
        newTipoRelacion () {
            vm.tipoRelacion = {};
            $('#frm-tipoRelacion').modal('show');
        },
        showTipoRelacion (id) {
            axios.post( urlShowTipoRelacion, { id: id })
                .then ( result => {
                    response = result.data;
                    vm.tipoRelacion = response.data;
                    $('#view-tipoRelacion').modal('show');
                })
                .catch ( error => {
                    console.log( error );
                });
        },
        editTipoRelacion () {
            $('#frm-tipoRelacion').modal('show');  
            $('#view-tipoRelacion').modal('hide');
        },
        saveTipoRelacion () {
            axios.post( urlSaveTipoRelacion, vm.tipoRelacion)
                .then ( result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    //$('#view-tipoRelacion').modal('show');
                    $('#frm-tipoRelacion').modal('hide');
                    var tipoRelacionTabla = $('#tipoRelacion-table').DataTable();
                    tipoRelacionTabla.draw();
                })
                .catch( error => {
                    vm.errorBag = error.data.errors;
                });
        },
        deleteTipoRelacion () {

            swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.post( urlDestroyTipoRelacion, {id : vm.tipoRelacion.id} )
                        .then( result => {
                            response = result.data;
                            toastr.success(response.msg, 'Correcto!');
                            var tipoRelacionTabla = $('#tipoRelacion-table').DataTable();
                            tipoRelacionTabla.draw();
                            $('#view-tipoRelacion').modal('hide');
                        })
                        .catch( error => {
                            console.log ( error );
                        })
                } else {
                  //swal("Your imaginary file is safe!");
                }
              });
        }
    },
});