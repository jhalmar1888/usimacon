$(function() {
    var GestionTabla = $('#gestion-table').DataTable({
        processing: true,
        order: [[1, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexGestion
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'id', orderable: false, searchable: false , visible: false},
            { data: 'Gestion', name: 'Gestion', title: 'Gestion' },
            { data: 'Monto', name: 'Monto', title: 'Monto' },            
            { data: 'Observaciones', name: 'Observaciones', title: 'Observaciones' },            
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? val : '', true, false).draw();
                    });                             
            });
        },
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#gestion-table tbody').on('click', 'tr', function () {
        var data = GestionTabla.row( this ).data();
        vm.$options.methods.showGestion(data.id);
    });
});

var vm = new Vue({
    el: '#gestion-app',
    data: {
        accounting: accounting,
        auth: auth,
        errorBag: {},
        isLoading: false,
        gestion: {},
    },
    methods: {
        newGestion () {
            vm.gestion = {};
            $('#frm-gestion').modal('show');
        },
        showGestion (id) {
            axios.post( urlShowGestion, { id: id })
                .then ( result => {
                    response = result.data;
                    vm.gestion = response.data;
                    $('#view-gestion').modal('show');
                })
                .catch ( error => {
                    console.log( error );
                });
        },
        editGestion () {
            $('#frm-gestion').modal('show');  
            $('#view-gestion').modal('hide');
        },
        saveGestion () {
            axios.post( urlSaveGestion, vm.gestion)
                .then ( result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-gestion').modal('hide');  
                    var gestionTabla = $('#gestion-table').DataTable();
                    gestionTabla.draw();
                })
                .catch( error => {
                    toastr.error('Ocurrió un error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },
        deleteGestion () {
            swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.post( urlDestroyGestion, {id : vm.gestion.id} )
                        .then( result => {
                            response = result.data;
                            toastr.success(response.msg, 'Correcto!');
                            var gestionTabla = $('#gestion-table').DataTable();
                            gestionTabla.draw();
                            $('#view-gestion').modal('hide');
                        })
                        .catch( error => {
                            console.log ( error );
                        })
                } else {
                  //swal("Your imaginary file is safe!");
                }
              });
        },
        generatePagosGestion () {
            swal({
                title: "Estas seguro que deseas generar pagos para ésta gestión?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.post( urlGeneratePagosGestion, {id : vm.gestion.id} )
                        .then( result => {
                            response = result.data;
                            toastr.success(response.msg, 'Correcto!');
                            vm.gestion = response.data;                            
                        })
                        .catch( error => {
                            console.log ( error );
                        })
                } else {
                  //swal("Your imaginary file is safe!");
                }
              });
        }
    },
});