
var vm = new Vue({
  el: "#dashboard-app",
  data: {
    //accounting: accounting,
    auth: auth,
    errorBag: {},
    isLoading: false,
    isLoadingFile: false,
   
    afiliados: {},
    movilidades: {},
    tipoVehiculo: {},
    personaPagos: {},
    
  },
  computed: {

  },
  methods: {

    getAfiliados() {
      axios
        .get(urlListAfiliado)
        .then(result => {
          response = result.data;
          vm.afiliados = response.data.length;
        })
        .catch(error => {
          console.log(error);
        });
    },
    getMovilidades() {
      axios
        .get(urlListPersonaMovilidad)
        .then(result => {
          response = result.data;
          vm.movilidades = response.data.length;
        })
        .catch(error => {
          console.log(error);
        });
    },
    getTipoVehiculo() {
      axios
        .get(urlListTipoVehiculo)
        .then(result => {
          response = result.data;
          vm.tipoVehiculo = response.data.length;
        })
        .catch(error => {
          console.log(error);
        });
    }, 
    getPersonaPago() {
      axios
        .get(urlListPersonaPagoGestion)
        .then(result => {
          response = result.data;
          vm.personaPagos = response.data;
        })
        .catch(error => {
          console.log(error);
        });
    },

  },
  mounted() {
    this.getAfiliados();
    this.getMovilidades();
    this.getTipoVehiculo();
    this.getPersonaPago();

  }
});

 


