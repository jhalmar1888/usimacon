$(function() {
    var TipoVehiculoTabla = $('#tipoVehiculo-table').DataTable({
        processing: true,
        order: [[1, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexTipoVehiculo
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'id', orderable: false, searchable: false , visible: false},
            { data: 'Num', name: 'Num', title: 'Número' },
            { data: 'TipoVehiculo', name: 'TipoVehiculo', title: 'TipoVehiculo' },
            
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column.search(val ? val : '', true, false).draw();
                });                             
            });
        },
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#tipoVehiculo-table tbody').on('click', 'tr', function () {
        var data = TipoVehiculoTabla.row( this ).data();
        vm.$options.methods.showTipoVehiculo(data.id);
    });
});

var vm = new Vue({
    el: '#tipoVehiculo-app',
    data: {
        //accounting: accounting,
        //auth: auth,
        errorBag: {},
        isLoading: false,
       
        tipoVehiculo: {},
    },
    methods: {
        newTipoVehiculo () {
            vm.tipoVehiculo = {};
            $('#frm-tipoVehiculo').modal('show');
        },
        showTipoVehiculo (id) {
            axios.post( urlShowTipoVehiculo, { id: id })
                .then ( result => {
                    response = result.data;
                    vm.tipoVehiculo = response.data;
                    $('#view-tipoVehiculo').modal('show');
                })
                .catch ( error => {
                    console.log( error );
                });
        },
        editTipoVehiculo () {
            $('#frm-tipoVehiculo').modal('show');  
            $('#view-tipoVehiculo').modal('hide');
        },
        saveTipoVehiculo () {
            axios.post( urlSaveTipoVehiculo, vm.tipoVehiculo)
                .then ( result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    //$('#view-tipoVehiculo').modal('show');
                    $('#frm-tipoVehiculo').modal('hide');
                    var TipoVehiculoTabla = $('#tipoVehiculo-table').DataTable();
                    TipoVehiculoTabla.draw();
                })
                .catch( error => {
                    vm.errorBag = error.data.errors;
                });
        },
        deleteTipoVehiculo () {

            swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    axios.post( urlDestroyTipoVehiculo, {id : vm.tipoVehiculo.id} )
                        .then( result => {
                            response = result.data;
                            toastr.success(response.msg, 'Correcto!');
                            var TipoVehiculoTabla = $('#tipoVehiculo-table').DataTable();
                            TipoVehiculoTabla.draw();
                            $('#view-tipoVehiculo').modal('hide');
                        })
                        .catch( error => {
                            console.log ( error );
                        })
                } else {
                  //swal("Your imaginary file is safe!");
                }
              });
        }
    },
});