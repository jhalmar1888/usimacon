$(function () {
    $("#FechaNacimiento")
    .datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      language: 'es'
    })
    .on("change", function() {
        vm.persona.FechaNacimiento = $("#FechaNacimiento").val();
    });
    
    $("#FechaIngreso")
    .datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        language: 'es'
    })
    .on("change", function() {
      vm.persona.FechaIngreso = $("#FechaIngreso").val();
    });

    var personaTabla = $('#persona-table').DataTable({
        processing: true,
        order: [[3, 'asc']],
        serverSide: true,
        ajax: {
            url: urlIndexPersona
        },
        deferRender: true,
        columns: [
            { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
            { data: 'Rol', name: 'r.Rol', title: 'Rol' },
            { data: 'CI', name: 'p.CI', title: 'CI' },
            { data: 'Persona', name: 'p.Persona', title: 'Nombre Completo' },
            { data: 'FechaNacimiento', name: 'p.FechaNacimiento', title: 'Fecha de Nacimiento' },
            { data: 'Celular', name: 'p.Celular', title: 'Celular' },
            // { data: 'email', name: 'p.email', title: 'Email' },
            { data: 'Placa', name: 'pm.Placa', title: 'Placa' },
            { data: 'TipoSocio', name: 'p.TipoSocio', title: 'Tipo Socio' },
            { data: 'Activo', name: 'p.Activo', title:'Estado',  render: function(data,type,row){
                if(row.Activo)
                    return '<i class="fa fa-check text-success"></i>'
                else    
                    return '<i class="fa fa-ban text-danger"></i>'
            }},

            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        language: { "url": "/lang/datatables.es.json" },
        dom: 'lftip',
    });

    $('#persona-table tbody').on('click', 'tr', function () {
        var data = personaTabla.row(this).data();
        vm.$options.methods.showPersona(data.id);
    });
});

var vm = new Vue({
    el: '#persona-app',
    data: {
        moment: moment,
        accounting: accounting,
        auth: auth,
        errorBag: {},
        isLoading: false,
        persona: {},
        fotografia: null,
        expedidos: expedidos,
        categoriaLicencias: categoriaLicencias,
        grupoSanguineos: grupoSanguineos,
        estadoCivils: estadoCivils,
        tipoSocios: tipoSocios,
        roles: {},
        sectors: {},
        isEditing: false,
        isLoadingFile: false
    },
    methods: {
        getRoles () {
            axios.get( urlListRol )
                .then( result => {
                    response = result.data;
                    vm.roles = response.data; 
                })
                .catch( error => {
                    console.log( error );
                })
        },
        getSector () {
            axios.get( urlListSector )
                .then( result => {vm.sectors = result.data.data})
                .catch( error => console.log( error ));
        },
        newPersona() {
            vm.persona = {};
            vm.persona.CategoriaLicencia = [];
            $('#frm-persona').modal('show');
        },
        uploadImage () {
            vm.isLoadingFile = true;
            var input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = (e) => {
                    this.fotografia = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
                var data = new FormData();
                data.append('File', input.files[0]);
                axios.post(urlUploadFile, data)
                    .then( result => {
                        if (result.data.success) {
                            toastr.info(result.data.msg, 'Correcto!');
                            vm.persona.Fotografia = result.data.data;
            vm.persona.CategoriaLicencia = vm.persona.CategoriaLicencia ? vm.persona.CategoriaLicencia.split(','): [];
                            
                            vm.savePersona();
                        } else {
                            toastr.error(result.data.msg, 'Oops!');
                        }
                        vm.isLoadingFile = false;
                    })
                    .catch( error => {
                        vm.isLoadingFile = false;
                        toastr.error('Error subiendo archivo', 'Oops!');
                    });
            }
        },
        showPersona(id) {
            axios.get(urlShowPersona, { params: { id: id }})
                .then(result => {
                    response = result.data;
                    vm.persona = response.data;
                    $('#view-persona').modal('show');
                })
                .catch(error => {
                    console.log(error);
                });
        },
        editPersona() {
            vm.persona.CategoriaLicencia = vm.persona.CategoriaLicencia ? vm.persona.CategoriaLicencia.split(','): [];
            $('#view-persona').modal('hide');
            $('#frm-persona').modal('show');
        },
        savePersona() {
            axios.post(urlSavePersona, vm.persona)
                .then(result => {
                    response = result.data;
                    toastr.success(response.msg, 'Correcto!');
                    $('#frm-persona').modal('hide');
                    vm.showPersona(response.data.id);
                    $('#view-persona').modal('show');
                    var personaTabla = $('#persona-table').DataTable();
                    personaTabla.draw();
                })
                .catch(error => {
                    console.log(error);
                    toastr.error('Error al guardar el registro', 'Oops!');
                    vm.errorBag = error.data.errors;
                });
        },
        
        deletePersona() {
            swal({
                title: "Estas seguro que deseas eliminar el registro? Recuerda que solo se eliminara sus Documentos Personales y familiares",
                text: "Esta accion es irreversible!  -----  Recuerda que debes Eliminar vehiculos de manera independiente       -----      a su vez los cargos que tuvo esta persona    ------ te recomendamos revisar en perfil estos dos módulos",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                confirmButtonText: 'Cool',
            })
            .then((willDelete) => {
                if (willDelete) {
                    axios.post(urlDestroyPersona, { id: vm.persona.id })
                        .then(result => {
                            response = result.data;
                            if ( response.success ) {
                                toastr.success(response.msg, 'Correcto!');
                                var personaTabla = $('#persona-table').DataTable();
                                personaTabla.draw();
                                $('#view-persona').modal('hide');
                            } else {
                                toastr.error(response.msg, 'Oops!');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        })
                } else {
                }
            });
        },
        goProfile() {
            token =  Math.random().toString(36).substring(2, 25) + Math.random().toString(36).substring(2, 35) + Math.random().toString(36).substring(2, 35);
            //console.log (token);
            window.location.replace('/Persona/profile?token=' + token + '&id=' + vm.persona.id);
        },
        imprimirRegistro() {
            axios.get( urlImprimirRegistro, { params: {id: vm.persona.id }} )
                .then( result => { 
                    response = result.data;
                    var urlFile = response.data.url;
                    // window.open(urlFile);
                    var x = new XMLHttpRequest();
                    x.open("GET", urlFile, true);
                    x.responseType = 'blob';
                    x.onload = e => {
                        //vm.isLoading = false; 
                        download(x.response, vm.persona.Nombre + ' ' + vm.persona.ApPaterno , "application/pdf" ); 
                    }
                    x.send();
                })
                .catch( error => {
                    console.log( error );
                });
        },
    },
    mounted () {
        this.getRoles();
        this.getSector();
    } 
});