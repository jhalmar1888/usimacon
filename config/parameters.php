
<?php

return [
    'org_name' => 'EMI',
    'web_page' => 'www.emi.edu.bo',
    'date_format' => env('DATE_FORMAT', 'Y-m-d H:i:s'),
    'date_format_insert' => env('DATE_FORMAT_INSERT', 'Y-m-d H:i:s'),
    'app_url' => env('APP_URL','http://planificacion.172.16.3.24.xip.io'),
    'generated_files' => env('APP_GENERATED_FILES','http://planificacion.172.16.3.24.xip.io/tmp/'),
    'url_logo' => env('URL_LOGO','http://planificacion.172.16.3.24.xip.io/storage/assets/logo.png'),
    'auth_office365' => false,
    'auth_2step' => false,
    'auth_google_recaptcha' => false,
    'allow_data' => true,

    'requisitos' => [
        'Hoja de Vida',
        'Fotocopia CI',
        'Fotocopia RUA',
        'Fotocopia Licencia de Conducir', 
        'Solicitud de Ingreso', 
        'Fotocopia SOAT'
    ]

    

];