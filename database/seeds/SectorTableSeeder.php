<?php

use Illuminate\Database\Seeder;

class SectorTableSeeder extends Seeder
{
      public function run()
    {
        DB::table('Sector')->insert(['Num'=> 1, 'Sector' => 'VLTAS - CMNES', 'ZonaSector' => 'CENTRAL SUCRE']);
        DB::table('Sector')->insert(['Num'=> 2, 'Sector' => 'CMTAS',  'ZonaSector' => 'CENTRAL SUCRE']);
        DB::table('Sector')->insert(['Num'=> 3, 'Sector' => 'VLTAS - CMNES', 'ZonaSector' => 'CEMENTERIO']);
        DB::table('Sector')->insert(['Num'=> 4, 'Sector' => 'VLTAS - CMNES','ZonaSector' => 'ACHACHICALA']);
        DB::table('Sector')->insert(['Num'=> 5, 'Sector' => 'VLTAS - CMNES',    'ZonaSector' => 'VILLA FATIMA']);
        DB::table('Sector')->insert(['Num'=> 6, 'Sector' => 'VLTAS - CMNES - CMNTAS',    'ZonaSector' => 'TEMBLADERANI']);
        DB::table('Sector')->insert(['Num'=> 7, 'Sector' => 'VLTAS - CMNES',    'ZonaSector' => 'SAN ANTONIO']);
        DB::table('Sector')->insert(['Num'=> 8, 'Sector' => 'CMTAS',    'ZonaSector' => 'CEMENTERIO']);
        DB::table('Sector')->insert(['Num'=> 9, 'Sector' => 'CMTAS',    'ZonaSector' => 'OBRAJES - CALACOTO']);
        DB::table('Sector')->insert(['Num'=> 10, 'Sector' => 'CMTAS',    'ZonaSector' => 'ESTEBAN ARCE']);
        DB::table('Sector')->insert(['Num'=> 11, 'Sector' => 'CMTAS',    'ZonaSector' => 'TEMBLADERANI']);
        DB::table('Sector')->insert(['Num'=> 12, 'Sector' => 'VLTAS - CMNES - CMNTAS',    'ZonaSector' => 'VILLA ARMONIA']);
        DB::table('Sector')->insert(['Num'=> 13, 'Sector' => 'VLTAS - CMNES',    'ZonaSector' => 'ALTO OBRAJES']);
        DB::table('Sector')->insert(['Num'=> 14, 'Sector' => 'CMTAS',    'ZonaSector' => 'INCA LLOJETA']);
        DB::table('Sector')->insert(['Num'=> 15, 'Sector' => 'VLTAS - CMNES',    'ZonaSector' => 'BAJO LLOJETA']);
        DB::table('Sector')->insert(['Num'=> 16, 'Sector' => 'TAXIS',    'ZonaSector' => 'ADEPCOCA']);
        DB::table('Sector')->insert(['Num'=> 17, 'Sector' => 'VLTAS - CMNES',    'ZonaSector' => 'IRPAVI II']);
        DB::table('Sector')->insert(['Num'=> 18, 'Sector' => 'TAXIS',    'ZonaSector' => 'FEMCONCOCA']);
        DB::table('Sector')->insert(['Num'=> 19, 'Sector' => 'VLATS - RETROEX',    'ZonaSector' => 'PORTADA - BOLMAR']);
        DB::table('Sector')->insert(['Num'=> 20, 'Sector' => 'TAXIS',  'ZonaSector' => 'FERROVIARIO']);
        DB::table('Sector')->insert(['Num'=> 21, 'Sector' => 'CMTAS',  'ZonaSector' => 'SECTOR PAMPAHASI']);
        DB::table('Sector')->insert(['Num'=> 22, 'Sector' => 'TAXIS',  'ZonaSector' => 'AMISTAD']);
        DB::table('Sector')->insert(['Num'=> 23, 'Sector' => 'CMNES',  'ZonaSector' => 'SECTOR CHUQUISACA']);

    }
}
