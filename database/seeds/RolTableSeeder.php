<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Rol')->insert(['Num'=>1, 'Rol' => 'Administrador']);
        DB::table('Rol')->insert(['Num'=>2, 'Rol' => 'Afiliado']);
    }
}