<?php

use Illuminate\Database\Seeder;

class PersonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Persona')->insert([
            'Rol' => 1,
            'CI' => '001122334455',
            'ApPaterno' => 'Del',
            'ApMaterno' => 'Sistema',
            'Nombre' => 'Administrador',
            'Persona' => 'Administrador del Sistema',
            'email' => 'admin@change.me',
            'password' => bcrypt('secret'),
            'Verificado' => 1,
            'Activo' => 1
        ]);
    }
}
