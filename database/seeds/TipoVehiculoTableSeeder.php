<?php

use Illuminate\Database\Seeder;

class TipoVehiculoTableSeeder extends Seeder
{
      public function run()
    {
        DB::table('TipoVehiculo')->insert(['Num'=> 1, 'TipoVehiculo' => 'Volquetas']);
        DB::table('TipoVehiculo')->insert(['Num'=> 2, 'TipoVehiculo' => 'Camiones']);
        DB::table('TipoVehiculo')->insert(['Num'=> 3, 'TipoVehiculo' => 'Taxis']);
        DB::table('TipoVehiculo')->insert(['Num'=> 4, 'TipoVehiculo' => 'Camionetas']);
        DB::table('TipoVehiculo')->insert(['Num'=> 5, 'TipoVehiculo' => 'Mixtas']);
    }
}
