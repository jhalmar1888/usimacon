<?php

use Illuminate\Database\Seeder;

class CargoTableSeeder extends Seeder
{
       public function run()
    {
        DB::table('Cargo')->insert(['Num'=> 1, 'Cargo' => 'STRIO. GENERAL', 'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 2, 'Cargo' => 'STRIO. RELACIONES',  'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 3, 'Cargo' => 'STRIO. HACIENDA',     'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 4, 'Cargo' => 'STRIO. ACTAS','TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 5, 'Cargo' => 'STRIO. CONFLICTOS',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 6, 'Cargo' => 'STRIO. REGIMEN INTERNO',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 7, 'Cargo' => 'STRIO. BENIFICIENCIA',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 8, 'Cargo' => 'STRIO. PREN. Y PROP.',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 9, 'Cargo' => 'STRIO. CULTURA',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 10, 'Cargo' => 'STRIO. DEPORTES',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 11, 'Cargo' => 'STRIO. COOPERATIVAS',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 12, 'Cargo' => 'STRIO. ABANDERADO',    'TipoCargo' => 'INSTITUCION']);
        DB::table('Cargo')->insert(['Num'=> 13, 'Cargo' => 'JEFE DE SECTOR',    'TipoCargo' => 'SECTOR']);
        DB::table('Cargo')->insert(['Num'=> 14, 'Cargo' => 'SUB JEFE DE SECTOR',    'TipoCargo' => 'SECTOR']);
        DB::table('Cargo')->insert(['Num'=> 15, 'Cargo' => 'STRIO. HACIENDA',    'TipoCargo' => 'SECTOR']);
        DB::table('Cargo')->insert(['Num'=> 16, 'Cargo' => 'STRIO. DE ACTAS',    'TipoCargo' => 'SECTOR']);
        DB::table('Cargo')->insert(['Num'=> 17, 'Cargo' => 'STRIO. DEPORTES',    'TipoCargo' => 'SECTOR']);
        DB::table('Cargo')->insert(['Num'=> 18, 'Cargo' => 'STRIO. PREN. Y PROP.',    'TipoCargo' => 'SECTOR']);
        DB::table('Cargo')->insert(['Num'=> 19, 'Cargo' => 'PORTA ESTANDARTE',    'TipoCargo' => 'SECTOR']);
    }
}
