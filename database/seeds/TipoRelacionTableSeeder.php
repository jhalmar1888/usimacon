<?php

use Illuminate\Database\Seeder;

class TipoRelacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TipoRelacion')->insert(['Num'=> 1, 'TipoRelacion' => 'Esposo (a)']);
        DB::table('TipoRelacion')->insert(['Num'=> 2, 'TipoRelacion' => 'Hijo (a)']);
        DB::table('TipoRelacion')->insert(['Num'=> 3, 'TipoRelacion' => 'Hermano (a)']);
        DB::table('TipoRelacion')->insert(['Num'=> 3, 'TipoRelacion' => 'Primo (a)']);
    }
}
