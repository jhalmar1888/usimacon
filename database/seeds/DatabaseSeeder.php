<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(TipoRelacionTableSeeder::class);
        $this->call(RolTableSeeder::class);        
        $this->call(PersonaTableSeeder::class);
        $this->call(TipoVehiculoTableSeeder::class);
        $this->call(CargoTableSeeder::class);
        $this->call(SectorTableSeeder::class);
    }
}
