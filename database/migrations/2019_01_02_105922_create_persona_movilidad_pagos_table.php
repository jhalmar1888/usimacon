<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaMovilidadPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonaPago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Persona')->unsigned();
            $table->integer('Gestion')->unsigned()->nullable();
            $table->integer('Mes')->nullable();
            $table->float('Monto')->default(0);
            $table->date('FechaPago')->nullable();
            $table->boolean('Pagado')->default('false');
            $table->boolean('Condonado')->default('false');
            $table->integer('NumeroRecibo')->nullable();
            $table->text('Observaciones')->nullable();

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            $table->foreign('Gestion')->references('id')->on('Gestion');
            $table->foreign('Persona')->references('id')->on('Persona');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonaPago');
    }
    
}
