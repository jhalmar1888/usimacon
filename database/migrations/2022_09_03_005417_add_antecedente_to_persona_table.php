<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAntecedenteToPersonaTable extends Migration
{

    public function up()
    {
        Schema::table('Persona', function (Blueprint $table) {
            $table->string('Antecedente', 700)->nullable();
            $table->string('Anexo', 500)->nullable();
            $table->integer('Auxiliar')->unsigned()->nullable();

        });
    }

    public function down()
    {
        Schema::table('Persona', function (Blueprint $table) {
            $table->dropColumn('Antecedente');
            $table->dropColumn('Anexo');
            $table->dropColumn('Auxiliar');
        });
    }
}
