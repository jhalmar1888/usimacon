<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRadicatoriaToPersonamovilidadTable extends Migration
{
    public function up()
    {
        Schema::table('PersonaMovilidad', function (Blueprint $table) {
            $table->string('Radicatoria', 500)->nullable();
            //$table->integer('Radicatoria')->unsigned()->nullable();
            $table->integer('Auxiliar')->unsigned()->nullable();
            $table->boolean('Revisado')->default(false);
            $table->string('Fotografiamovi', 500)->nullable();
            $table->string('Antecedentes', 500)->nullable();


            //$table->foreign('Radicatoria')->references('id')->on('Radicatoria');
        });
    }

    public function down()
    {
        Schema::table('PersonaMovilidad', function (Blueprint $table) {
            $table->dropColumn('Radicatoria');
            $table->dropColumn('Auxiliar');
            $table->dropColumn('Revisado');
            $table->dropColumn('Fotografiamovi');
            $table->dropColumn('Antecedentes');
        });
    }
}
