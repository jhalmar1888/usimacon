<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Gestion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Gestion')->unsigned();
            $table->float('Monto')->default(0);
            $table->text('Observaciones')->nullable();
            $table->boolean('PagosGenerados')->default(false);
            $table->integer('CorrelativoRecibo')->default(0);
            $table->string('Sgeneral', 250)->nullable();
            $table->string('Shacienda', 250)->nullable();
            $table->string('Sconflicto', 250)->nullable();
            $table->string('Sregimen', 250)->nullable();

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Gestion');
    }
}
