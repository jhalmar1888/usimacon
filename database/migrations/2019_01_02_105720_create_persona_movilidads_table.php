<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaMovilidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonaMovilidad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Persona')->unsigned();
            $table->integer('TipoVehiculo')->unsigned();
            $table->string('Marca', 150)->nullable();
            $table->string('Capacidad')->unsigned();
            $table->string('Color')->unsigned();
            $table->string('NumeroChasis')->unsigned();
            $table->integer('Modelo')->unsigned();
            $table->string('Placa')->nullable();
            $table->string('RUA')->nullable();
            $table->text('Observaciones')->nullable();
            $table->boolean('Activo')->default(false);

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            $table->foreign('Persona')->references('id')->on('Persona');
            $table->foreign('TipoVehiculo')->references('id')->on('TipoVehiculo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonaMovilidad');
    }
}
