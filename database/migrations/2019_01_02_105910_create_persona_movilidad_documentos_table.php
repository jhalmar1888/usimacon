<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaMovilidadDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PersonaMovilidadDocumento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('PersonaMovilidad')->unsigned();
            $table->string('PersonaMovilidadDocumento', 250); //nombre del documento que registra
            $table->string('Archivo', 150)->nullable();
            $table->date('FechaInicioVigencia')->nullable();
            $table->date('FechaFinVigencia')->nullable();
            $table->text('Observaciones')->nullable();

            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            $table->foreign('PersonaMovilidad')->references('id')->on('PersonaMovilidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonaMovilidadDocumento');
    }
}
