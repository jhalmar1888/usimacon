<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Persona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Persona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Rol')->unsigned()->nullable();
            $table->string('ApPaterno',50)->nullable();
            $table->string('ApMaterno',50)->nullable();
            $table->string('Nombre',50);
            $table->string('Persona', 550)->nullable()->index();
            $table->string('CI', 25)->index();
            $table->string('LibretaMilitar', 50)->nullable();
            $table->string('Expedido', 5)->nullable();
            $table->date('FechaNacimiento')->nullable();
            $table->string('Sexo', 1)->nullable();
            $table->string('EstadoCivil', 20)->nullable();
            $table->string('TipoSocio', 20)->nullable();
            $table->string('Direccion',255)->nullable();
            $table->string('Telefono',20)->nullable();
            $table->string('Celular',20)->nullable();
            $table->string('GrupoSanguineo',20)->nullable();
            $table->string('CategoriaLicencia', 25)->nullable();
            $table->string('Fotografia',50)->nullable();
            $table->string('Observaciones',255)->nullable();
            $table->boolean('Activo')->default(true);
            $table->date('FechaIngreso')->nullable();
            $table->integer('SectorActual')->unsigned()->nullable();

            /* credenciales de acceso al sistema */
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->boolean('Verificado')->default(false);
            $table->string('TokenVerificacion', 150)->index()->nullable();
            $table->string('TokenLogin')->nullable();
            $table->rememberToken();
            
            $table->nullableTimestamps();
            $table->SoftDeletes();
            $table->string('CreatorUserName', 250)->nullable();
            $table->string('CreatorFullUserName', 250)->nullable();
            $table->string('CreatorIP', 250)->nullable();
            $table->string('UpdaterUserName', 250)->nullable();
            $table->string('UpdaterFullUserName', 250)->nullable();
            $table->string('UpdaterIP', 250)->nullable();
            $table->string('DeleterUserName', 250)->nullable();
            $table->string('DeleterFullUserName', 250)->nullable();
            $table->string('DeleterIP', 250)->nullable();

            $table->foreign('Rol')->references('id')->on('Rol');
        });
        //
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Persona'); //
    }
}
