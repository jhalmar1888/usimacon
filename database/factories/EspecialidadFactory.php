<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Especialidad::class, function (Faker $faker) {
    return [
        'UnidadAcademica' => $faker->numberBetween(1,5) ,
        'NivelAcademico' => $faker->numberBetween(1,3) ,
        'Especialidad' => $faker->words(4, true) ,
        'Leyenda' => $faker->randomHtml(2,3)
    ];
});
