<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Requisito::class, function (Faker $faker) {
    $especialidad = $faker->randomElement(array($faker->numberBetween(1,50) ,null));
    $beca = $faker->randomElement(array($faker->numberBetween(1,4),null));
    
    if($especialidad != null)
        $beca = null;
    elseif($beca == null)
            $beca = $faker->numberBetween(1,4);
    
    return [
        'Especialidad' => $especialidad,
        'Beca' => $beca,
        'Requisito' => $faker->words(5, true)
    ];
});
