<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Beca::class, function (Faker $faker) {
    return [
        'Beca' => $faker->word
    ];
});
