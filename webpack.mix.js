let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

/****************************************************************************
                              Codigo Añadido                                    
==========================================================================*/

var CopyWebpackPlugin = require('copy-webpack-plugin');
var UglifyJS = require('uglify-es');

if (mix.config.production) {
    mix.webpackConfig( webpack => {
        console.log("aca");
        return {
            plugins: [
                new CopyWebpackPlugin([
                    // Copy glob results (with dot files) to /absolute/path/
                    {
                        from: 'resources/assets/js/modules/',
                        to: '../public/js/',
                        transform (content, path) {
                            return Promise.resolve(Buffer.from(UglifyJS.minify(content.toString()).code, 'utf8'));
                            // return optimize(content);
                        }
                    },
                ], 
                {
                    copyUnmodified: true
                })
            ]
        }
    });
} else {
    mix.copyDirectory('resources/assets/js/modules','public/js')
    mix.browserSync({
        proxy: 'http://usimacon.test'
    });
}

































